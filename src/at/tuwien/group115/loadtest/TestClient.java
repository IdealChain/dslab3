package at.tuwien.group115.loadtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.LoadTestProperties;

public class TestClient implements Runnable {

	private final static Logger log = Logger.getLogger(TestClient.class);
	
	private LoadTestManager parent;
	private String name;
	private boolean isStopped = false;
	private Socket socket;
	
	public TestClient(LoadTestManager parent, String name) {
		this.parent = parent;
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	@Override
	public void run() {
		
		PrintWriter out = null;
		BufferedReader in = null;
		
		try {
			socket = new Socket(parent.getServer(), parent.getTCPPort());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			
			this.simulateAuction(in,  out);			
			
		} catch (ConnectException e) {
			
			if (this.getIsStopped())
				return;
			
			String msg = String.format("%s could not connect (to %s:%d): %s", name, parent.getServer(), parent.getTCPPort(), e.getMessage());
			log.fatal(msg);
			faulted(msg);
			return;
			
		} catch (Exception e) {
			
			if (this.getIsStopped())
				return;
			
			log.fatal(name + " error", e);
			faulted(e.getMessage());
			return;
			
		} finally {
			try {
				if (socket != null)
					socket.close();
			} catch (IOException e) {
				log.warn("IOException during close", e);
			}
		}
	}
	
	private void faulted(String msg) {
		parent.testClientFaulted(this, msg);
	}
	
	private void simulateAuction(BufferedReader in, PrintWriter out) throws InterruptedException, IOException {
		
		LoadTestProperties testProp = parent.getTestProperties();
		Random random = new Random();
		ArrayList<String> runningAuctions = null;
		
		// calculate auction and bid probability per interval
		long wakeupInterval = 500;
		double auctionsPerInterval = testProp.getAuctionsPerMin() * wakeupInterval / 60000.0;
		double bidsPerInterval = testProp.getBidsPerMin() * wakeupInterval / 60000.0;
		int updateListIntervals = (int)(testProp.getUpdateIntervalSec() * (1000.0 / wakeupInterval));
		
		TestTerminal terminal = new TestTerminal(in, out);
		
		// randomize thread starts
		Thread.sleep(random.nextInt(30000));
		if (this.getIsStopped())
			return;
		
		log.info(name + " logging in");
		if (!terminal.login(name)) {
			faulted(name + " could not login");
			return;
		}
		
		int updateListRemaining = 0;
		while (!this.getIsStopped()) {
			
			// every updateIntervalSecs: update list of auction ids
			if (updateListRemaining < 1) {
				runningAuctions = terminal.getRunningAuctions();				
				updateListRemaining = updateListIntervals;
			}
			
			// with probability auctionsPerInterval: create new auction
			if (random.nextDouble() < auctionsPerInterval) {
				log.info(name + " creating new auction");
				terminal.createAuction(testProp.getAuctionDuration(), "TestAuction");
			}
			
			// with probability bidsPerInterval: bid on random auction id
			if (random.nextDouble() < bidsPerInterval) {
				if (runningAuctions.size() > 0) {
					String id = runningAuctions.get(random.nextInt(runningAuctions.size()));
					double amount = parent.getElapsedSeconds();
					
					log.info(String.format("%s bidding on auction %s with %.2f", name, id, amount));
					terminal.bid(id, amount);
				}
			}
			
			Thread.sleep(wakeupInterval);
			updateListRemaining--;
		}
		
		out.println("!logout");
	}
	
	private synchronized boolean getIsStopped() {
		return this.isStopped;
	}
	
	public synchronized void stop() {
		this.isStopped = true;
		if (this.socket != null) {
			try {
				this.socket.close();
			} catch (IOException e) {
				log.warn("IOException during close", e);
			}
		}
	}
}
