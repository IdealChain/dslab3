package at.tuwien.group115.loadtest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class TestTerminal {
	
	private final static Logger log = Logger.getLogger(TestTerminal.class);

	private BufferedReader in;
	private PrintWriter out;
	
	public TestTerminal(BufferedReader in, PrintWriter out) {
		this.in = in;
		this.out = out;
	}
	
	
	protected boolean login(String name) throws IOException {
		out.println(String.format(Locale.ENGLISH, "!login %s", name));
		return checkLoggedIn(readResult(), name);
	}
	
	protected ArrayList<String> getRunningAuctions() throws IOException {
		out.println("!list");
		return parseAuctions(readResult());
	}
	
	protected void createAuction(int duration, String desc) throws IOException {
		out.println(String.format(Locale.ENGLISH, "!create %d %s", duration, desc));
		readResult(); // consume returned lines
	}
	
	protected void bid(String auctionId, double amount) throws IOException {
		out.println(String.format(Locale.ENGLISH, "!bid %s %.2f", auctionId, amount));
		readResult(); // consume returned lines
	}
	
	
	private ArrayList<String> readResult() throws IOException {
		ArrayList<String> result = new ArrayList<String>();
		
		// read till empty line (=end of return)
		String line;
		while (!(line = in.readLine()).equals(""))
			result.add(line);
		
		return result;
	}
	
	private static Pattern loggedInPattern = Pattern.compile("([\\w ]*)(> )?Successfully logged in as (.*)!");
	
	private boolean checkLoggedIn(ArrayList<String> result, String username) {
		boolean success = false;
		String lines = "\n";
		
		for(String line : result) {
			lines += line + "\n";
			Matcher m = loggedInPattern.matcher(line);
			if (m.matches() && m.group(3).equals(username))
				success = true;
		}
		
		if (!success)
			log.warn(String.format("%s not logged in successfully:%s", username, lines));
		
		return success;		
	}
	
	private static Pattern auctionPattern = Pattern.compile("([\\w ]*)(> )?([0-9]+)\\. '(.*)' .*");
	
	private ArrayList<String> parseAuctions(ArrayList<String> result) {
		ArrayList<String> auctions = new ArrayList<String>();
		String lines = "\n";
		
		for(String line : result) {
			lines += line + "\n";
			Matcher m = auctionPattern.matcher(line);
			if (m.matches())
				auctions.add(m.group(3));
		}
		
		if (auctions.size() < 1)
			log.warn(String.format("Not seeing any active auctions:%s", lines));
		
		return auctions;
	}
}
