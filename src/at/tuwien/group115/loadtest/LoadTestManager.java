package at.tuwien.group115.loadtest;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import at.tuwien.group115.managementclient.SubscriptionManager;
import at.tuwien.group115.shared.LoadTestProperties;

public class LoadTestManager {
	
	private final static Logger log = Logger.getLogger(LoadTestManager.class);
	
	private String server;
	private int tcpPort;
	private String analyticsBindingName;
	private LoadTestProperties testProperties;
	
	private SubscriptionManager subManager;
	private ArrayList<TestClient> clients;
	private long startTimestamp;
	private boolean startedSuccessfully = false;
	
	private class FaultedClient {
		TestClient client;
		String fault;
		
		public FaultedClient(TestClient client, String fault) {
			this.client = client;
			this.fault = fault;
		}
		
		@Override
		public String toString() {
			return String.format("Client %s faulted: %s", client.getName(), fault);
		}
	}
	
	private ArrayList<FaultedClient> faultedClients = new ArrayList<FaultedClient>();
	
	public LoadTestManager(String server, int tcpPort, String analyticsBindingName, LoadTestProperties testProperties) {
		this.server = server;
		this.tcpPort = tcpPort;
		this.analyticsBindingName = analyticsBindingName;
		this.testProperties = testProperties;
	}

	public boolean run() {
		
		startTimestamp = new Date().getTime();
			
		// setup subscription manager
		subManager = new SubscriptionManager(analyticsBindingName);
		subManager.setAutoPrintingMode(true);
		String subscriptionId = subManager.subscribe(".*");
		if (subscriptionId == null) {
			log.warn("Could not subscribe - is analytics server running?");
			return false;
		}
		log.info(String.format("Subscribed to all events with ID %s", subscriptionId));
		
		// start client threads
		log.info(String.format("Starting up %d client threads...", testProperties.getClients()));
		clients = new ArrayList<TestClient>();
		for(int i = 0; i < testProperties.getClients(); i++) {
			
			String name = String.format("client%03d", i);
			TestClient client = new TestClient(this, name);
			clients.add(client);
			
			new Thread(client).start();
			
			// give each thread a bit time to start up
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {}
			
			// check if any thread faulted
			if (this.hasTestClientFaulted())
				break;
		}
		
		// if any thread faulted: startup not successful
		synchronized(this) {
			if (this.hasTestClientFaulted()) {
				stop();
				return false;
			}
			
			log.info("All client threads started successfully.");
			this.startedSuccessfully = true;		
			return true;
		}
	}
	
	public synchronized void testClientFaulted(TestClient client, String msg) {
		FaultedClient faulted = new FaultedClient(client, msg);
		log.info(String.format("%s, stopping loadtest...", faulted.toString()));
		
		faultedClients.add(faulted);
		
		if (this.startedSuccessfully)
			stop();
	}
	
	public synchronized boolean hasTestClientFaulted() {
		return faultedClients.size() > 0;
	}
	
	public synchronized void stop() {
		
		if (subManager != null)
			subManager.cleanup();
		
		log.info("Stopping client threads...");
		for(TestClient client : clients)
			client.stop();
		
		clients.clear();
	}
	
	protected String getServer() {
		return this.server;
	}
	
	protected int getTCPPort() {
		return this.tcpPort;
	}
	
	protected LoadTestProperties getTestProperties() {
		return this.testProperties;
	}
	
	protected double getElapsedSeconds() {
		long elapsedMS = new Date().getTime() - startTimestamp;
		return elapsedMS / 1000.0;
	}
}
