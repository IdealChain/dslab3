package at.tuwien.group115.loadtest;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.LoadTestProperties;
import at.tuwien.group115.shared.PropertiesInvalidException;
import at.tuwien.group115.shared.Utility;

public class Main {
	
	private final static Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		String server = null;
		int tcpPort = 0;
		String analyticsBindingName = null;
		
		try {
			if (args.length >= 3) {
				server = args[0];
				tcpPort = Integer.parseInt(args[1]);
				analyticsBindingName = args[2];
			}
		} catch (NumberFormatException e) {}
		
		if (server == null || server.length() < 1)
			usage();
		
		if (tcpPort < 1 || tcpPort > 65535)
			usage();
		
		if (analyticsBindingName == null || analyticsBindingName.length() < 1)
			usage();
		
		// configure basic log4j console logger
		BasicConfigurator.configure();
		
		// read loadtest.properties
		LoadTestProperties testProp = null;
		try {
			testProp = new LoadTestProperties();
		} catch (PropertiesInvalidException e) {
			log.warn("loadtest.properties file invalid: " + e.getMessage(), e.getCause());
			System.exit(1);
		}
		
		LoadTestManager manager = new LoadTestManager(server, tcpPort, analyticsBindingName, testProp);
		
		if (!manager.run())
			System.exit(1);
		
		System.out.println("LoadTest running, enter !exit to quit.");
		Utility.waitForExitCommand();
		
		log.info("Stopping...");
		manager.stop();
	}
	
	public static void usage() {
		System.err.println(String.format("Usage: java %s <server> <tcp-port> <analytics-bindingName>", Main.class.getName()));
		System.exit(1);
	}

}
