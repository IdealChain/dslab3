package at.tuwien.group115.billing;

import java.io.Serializable;

/**
 * Represents an auction
 */
public class Auction implements Serializable
{
	/**
	 * serial id
	 */
	private static final long serialVersionUID = 1L;
	
	private String user = null;
	private Long auctionId;
	private Double price = 0.0;
	
	public Auction(String user, Long auctionId, Double price)
	{
		this.user = user;
		this.auctionId = auctionId;
		this.price = price;
	}
	
	public String getUser() {
		return user;
	}

	public Long getAuctionId() {
		return auctionId;
	}

	public Double getPrice() {
		return price;
	}
}
