package at.tuwien.group115.billing;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

public class PriceSteps implements Serializable
{
	/**
	 * serial id
	 */
	private static final long serialVersionUID = 1L;
	
	HashMap<Double, PriceStep> steps = null;
	
	public PriceSteps()
	{
		steps = new HashMap<Double, PriceStep>();
	}
	
	/**
	 * Adds a new price step to the collection if the
	 * specified step interval [start, end] is not already set.
	 * 
	 * @param start
	 * @param end
	 * @param fixedPrice
	 * @param variablePricePercent
	 * @return boolean
	 */
	public synchronized boolean addPriceStep(double start, double end, double fixedPrice, double variablePricePercent)
	{
		// start price already exists
		if (steps.get(start) != null)
			return false;
		
		Iterator<PriceStep> i = steps.values().iterator();
		while (i.hasNext())
		{
			PriceStep s = i.next();
			
			// end price already exists
			if (end == s.getEnd())
				return false;
			
			if (0 == end)
			{
				if (start < s.getEnd())
					return false;
				continue;
			}
			
			if (start > end)
				return false;
			
			// price is in existing interval
			if (start > s.getStart() && end < s.getEnd())
				return false;
			if (start < s.getStart() && end > s.getStart())
				return false;
			if (start > s.getStart() && s.getEnd().equals(Double.parseDouble("0")))
				return false;
		}
		steps.put(start, new PriceStep(start, end, fixedPrice, variablePricePercent));
		return true;
	}
	
	/**
	 * Removes a price step from the collection
	 * 
	 * @param start the start price
	 * @param end the end price
	 * @return boolean
	 */
	public synchronized boolean removePriceStep(double start, double end)
	{
		PriceStep step = steps.get(start);
		if (step != null && step.getEnd().equals(end))
		{
			steps.remove(start);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Returns the price steps
	 * 
	 * @return
	 */
	public HashMap<Double, PriceStep> getPriceSteps()
	{
		return steps;
	}
}
