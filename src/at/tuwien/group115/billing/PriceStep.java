package at.tuwien.group115.billing;

import java.io.Serializable;

public class PriceStep implements Serializable
{
	/**
	 * serial id
	 */
	private static final long serialVersionUID = 1L;
	
	private Double start;
	private Double end;
	private Double fixedPrice;
	private Double variablePricePercent;
	
	public PriceStep(Double start, Double end, Double fixedPrice, Double variablePricePercent)
	{
		this.start = start;
		this.end = end;
		this.fixedPrice = fixedPrice;
		this.variablePricePercent = variablePricePercent;
	}
	
	public Double getStart() {
		return start;
	}
	
	public Double getEnd()	{
		return end;
	}
	
	public Double getFixedPrice() {
		return fixedPrice;
	}
	
	public Double getvariablePricePercent() {
		return variablePricePercent;
	}
}
