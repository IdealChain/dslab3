package at.tuwien.group115.billing;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class BillingServerSecure extends UnicastRemoteObject implements IBillingServerSecure {

	/**
	 * serialid for serialization
	 */
	private static final long serialVersionUID = 1L;
	
	private final static Logger log = Logger.getLogger(BillingServerSecure.class);
	
	private PriceSteps priceSteps = null;
	private List<Auction> auctions = null;
	
	protected BillingServerSecure() throws RemoteException {
		super();
		
		priceSteps = new PriceSteps();
		auctions = new ArrayList<Auction>();
	}

	@Override
	public PriceSteps getPriceSteps() throws RemoteException {
		return priceSteps;
	}

	@Override
	public synchronized void createPriceStep(double startPrice, double endPrice,
			double fixedPrice, double variablePricePercent)	throws RemoteException 
	{
		if (startPrice < 0 || endPrice < 0)
		{
			log.info(String.format("Step [%d %d] could not be created because a step value must not be < 0", startPrice, endPrice));
			throw new RemoteException(String.format("Step [%d %d] could not be created because a step value must not be < 0", startPrice, endPrice));
		}
		
		boolean stepAdded = priceSteps.addPriceStep(startPrice, endPrice, fixedPrice, variablePricePercent);
		if (stepAdded)
			log.info(String.format("Step [%f %f] successfully added", startPrice, endPrice));
		else
		{
			log.info(String.format("Step [%f %f] could not be added because this interval is already in use", startPrice, endPrice));
			throw new RemoteException(String.format("Step [%f %f] could not be created because this interval is already in use", startPrice, endPrice));
		}
	}

	@Override
	public synchronized void deletePriceStep(double startPrice, double endPrice)
			throws RemoteException {
		
		boolean stepRemoved = priceSteps.removePriceStep(startPrice, endPrice);
		if (stepRemoved)
			log.info(String.format("Price step [%f %f] successfully removed", startPrice, endPrice));
		else
		{
			log.error(String.format("Price step [%f %f] does not exist", startPrice, endPrice));
			throw new RemoteException(String.format("Price step [%f %f] does not exist", startPrice, endPrice));
		}
	}

	@Override
	public synchronized void billAuction(String user, long auctionID, double price) throws RemoteException {
		Auction auction = new Auction(user, auctionID, price);
		auctions.add(auction);
		log.info(String.format("Auction %d from user '%s' with price '%f' received.", auctionID, user, price));
	}

	@Override
	public synchronized Bill getBill(String user) throws RemoteException
	{
		Bill bill = new Bill(priceSteps);

		for (Auction a : auctions)
		{
			log.info("Add auction  with id " + a.getAuctionId());
			if (!a.getUser().equals(user))
				continue;
			
			bill.addAuction(a);
		}
		return bill;
	}

}
