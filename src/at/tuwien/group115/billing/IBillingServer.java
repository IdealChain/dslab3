package at.tuwien.group115.billing;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IBillingServer extends Remote 
{
	IBillingServerSecure login(String username, String password) throws RemoteException;
}
