package at.tuwien.group115.billing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Bill implements Serializable
{
	/**
	 * serial id
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Auction> auctions = null;
	private PriceSteps priceSteps = null;
	
	public Bill(PriceSteps priceSteps)
	{
		auctions = new ArrayList<Auction>();
		this.priceSteps = priceSteps;
	}
	
	public synchronized void addAuction(Auction auction)
	{
		auctions.add(auction);
	}
	
	public synchronized List<Auction> getAuctions()
	{
		return auctions;
	}
	
	@Override
	public String toString()
	{
		String ret = "\tauction_ID\tstrike_price\tfee_fixed\tfee_variable\tfee_total\n";

		HashMap<Double, PriceStep> steps = priceSteps.getPriceSteps();
		
		for (Auction a : auctions)
		{
			Iterator<PriceStep> i = steps.values().iterator();
			double variablePrice = 0.0;
			double fixedPrice = 0.0;
			while (i.hasNext())
			{
				PriceStep step = i.next();
				if (step != null && a.getPrice() >= step.getStart() && 
				   (a.getPrice() <= step.getEnd() || step.getEnd() == 0))
				{
					// step found
					variablePrice = step.getvariablePricePercent();
					fixedPrice = step.getFixedPrice();
					break;
				}
				variablePrice = 0.0;
				fixedPrice = 0.0;
			}
			
			double fee_variable = a.getPrice() * (variablePrice / 100.0);
			double fee_total = fee_variable + fixedPrice;
			
			ret += String.format("\t\t%10d\t%12.2f\t%9.2f\t%12.2f\t%9.2f\n", a.getAuctionId(), a.getPrice(), 
					fixedPrice, fee_variable, fee_total);
		}
		return ret;
	}
}
