package at.tuwien.group115.billing;

import java.rmi.AlreadyBoundException;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.RegistryHelper;
import at.tuwien.group115.shared.PropertiesInvalidException;
import at.tuwien.group115.shared.Utility;

public class Main {
	
	private final static Logger log = Logger.getLogger(Main.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1)
			usage();

		// configure basic log4j console logger
		BasicConfigurator.configure();
		
		String bindingName = args[0];
		
		BillingServer server = null;
		
		// obtain RMI registry
		Registry registry = null;
		try {
			registry = RegistryHelper.getRegistry();
		} catch (PropertiesInvalidException e) {
			log.fatal("registry.properties file invalid: " + e.getMessage(), e.getCause());
			System.exit(1);
		} catch (RemoteException e) {
			log.fatal("Registry could not be obtained", e);
			System.exit(1);
		}
		
		// start server
		try {
			server = new BillingServer();
			registry.bind(bindingName, server);
		} catch (AlreadyBoundException e) {
			log.fatal("BillingServer already bound to rmi registry", e);
			System.exit(1);
		} catch (RemoteException e) {
			log.fatal("RemoteException during rmi registry setup", e);
			System.exit(1);
		}
		
		System.out.println("Server started, enter !exit to quit.");
		
		Utility.waitForExitCommand();
		log.info("Stopping...");
		
		// unexport event server so that rmi threads shut down
		try {
			if (server != null) {
				UnicastRemoteObject.unexportObject(server, true);
				server.cleanup();
			}
		} catch (NoSuchObjectException e) {
			log.warn("Server object could not be unexported", e);
		}
	}

	public static void usage() {
		System.err.println(String.format("Usage: java %s bindingName", BillingServer.class.getName()));
		System.exit(1);
	}
}
