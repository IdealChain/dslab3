package at.tuwien.group115.billing;

import java.math.BigInteger;
import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import at.tuwien.group115.shared.PropertiesInvalidException;
import at.tuwien.group115.shared.UserProperties;

public class BillingServer extends UnicastRemoteObject implements IBillingServer {

	/**
	 * serialid for serialization
	 */
	private static final long serialVersionUID = 1L;
	
	private final static Logger log = Logger.getLogger(BillingServer.class);
	
	private Map<String, String> users = null;
	private ArrayList<IBillingServerSecure> remoteObjects = new ArrayList<IBillingServerSecure>();
	private IBillingServerSecure sec = null;
	
	protected BillingServer() throws RemoteException {
		super();
	}

	@Override
	public synchronized IBillingServerSecure login(String username, String password) throws RemoteException
	{
		if (users == null)
		{
			UserProperties props = null;
			try {
				props = new UserProperties();
			} catch (PropertiesInvalidException e) {
				log.fatal(String.format("Could not parse user.properties file: %s", e.getMessage()), e);
			}
			users = props.getUsers();
		}
		
		// no user found, return null
		String pwd = users.get(username);
		if (pwd == null)
			return null;
		
		MessageDigest alg = null;
		try {
			alg = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			log.fatal(String.format("Could not find algorithmn: ", e.getMessage()), e);
		}
		alg.update(password.trim().getBytes());
		
		String md5 = new BigInteger(1, alg.digest()).toString(16);
		
		// passwords do not match
		if (!pwd.equals(md5))
			return null;

		try {
			if (sec == null)
			{
				sec = new BillingServerSecure();
				remoteObjects.add(sec);
			}
		} catch (RemoteException e) {
			log.fatal(String.format("Could not create BillingServerSecure: %s", e.getMessage()), e);
		}
		
		return sec;
	}
	
	public synchronized void cleanup() {
		if (remoteObjects.size() > 0)
			log.info(String.format("Cleaning %d remote objects...", remoteObjects.size()));
		
		for(IBillingServerSecure sec : remoteObjects) {

			try {
				if (sec != null)
					UnicastRemoteObject.unexportObject(sec, true);
			} catch (NoSuchObjectException e) {}
		}
		
		remoteObjects.clear();
	}
}
