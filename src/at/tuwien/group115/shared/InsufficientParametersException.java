package at.tuwien.group115.shared;

public class InsufficientParametersException extends Exception {

	private static final long serialVersionUID = 1L;
	public int ExpectedParameters = 0;

	public InsufficientParametersException(int expected) {
		this.ExpectedParameters = expected;
	}

	public InsufficientParametersException(String msg) {
		super(msg);
	}

}