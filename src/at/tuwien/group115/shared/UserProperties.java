package at.tuwien.group115.shared;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import org.apache.log4j.Logger;

public class UserProperties {
	
	private final static Logger log = Logger.getLogger(UserProperties.class);
	
	// Map<Username, Password>
	private HashMap<String, String> users = null;

	public UserProperties() throws PropertiesInvalidException {
		InputStream is = ClassLoader.getSystemResourceAsStream("user.properties");
		users = new HashMap<String, String>();
		
		if (is != null) {
			Properties props = new Properties();
			try {
				props.load(is);

				Enumeration<Object> e = props.keys();
				while (e.hasMoreElements())
				{
					String s = e.nextElement().toString();
					users.put(s, props.getProperty(s));
					log.info(String.format("added user %s", s));
				}
			} catch (IOException e) {
				throw new PropertiesInvalidException("IO error", e);
			} finally {
				try {
					is.close();
				} catch (IOException e) {}
			}
		} else {
			throw new PropertiesInvalidException("File not found");
		}
	}
	
	public HashMap<String, String> getUsers() {
		return users;
	}
}
