package at.tuwien.group115.shared;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class LoadTestProperties {
	
	private final static Logger log = Logger.getLogger(LoadTestProperties.class);
	
	private int clients;
	private int auctionsPerMin;
	private int auctionDuration;
	private int updateIntervalSec;
	private int bidsPerMin;
	
	public LoadTestProperties() throws PropertiesInvalidException {
		
		InputStream is = ClassLoader.getSystemResourceAsStream("loadtest.properties");
		
		if (is != null) {
			Properties props = new Properties();
			try {
				props.load(is);
				
				this.clients = Integer.parseInt(props.getProperty("clients"));
				this.auctionsPerMin = Integer.parseInt(props.getProperty("auctionsPerMin"));
				this.auctionDuration = Integer.parseInt(props.getProperty("auctionDuration"));
				this.updateIntervalSec = Integer.parseInt(props.getProperty("updateIntervalSec"));
				this.bidsPerMin = Integer.parseInt(props.getProperty("bidsPerMin"));
				
			} catch (IOException e) {
				throw new PropertiesInvalidException("IO error", e);
			} catch (NumberFormatException e) {
				throw new PropertiesInvalidException("Number not valid", e);
			} finally {
				try {
					is.close();
				} catch (IOException e) {}
			}
		} else {
			throw new PropertiesInvalidException("File not found");
		}
		
		log.info("loadtest.properties read");
	}
	
	public int getClients() {
		return clients;
	}

	public int getAuctionsPerMin() {
		return auctionsPerMin;
	}

	public int getAuctionDuration() {
		return auctionDuration;
	}

	public int getUpdateIntervalSec() {
		return updateIntervalSec;
	}

	public int getBidsPerMin() {
		return bidsPerMin;
	}

}
