package at.tuwien.group115.shared;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

public final class Utility {
	
	private final static Logger log = Logger.getLogger(Utility.class);
	
	public static String waitForCommand(List<String> allowed) {
		String in = null;
		try {
			BufferedReader inBR = new BufferedReader(new InputStreamReader(System.in));
			do {
				in = inBR.readLine();
			} while (in != null && !allowed.contains(in));
		} catch (IOException e) { 
			log.warn("IOException", e); 
		}
		return in;
	}
	
	public static void waitForExitCommand() {
		waitForCommand(Arrays.asList("!end", "!exit"));
	}

}
