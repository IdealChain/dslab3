package at.tuwien.group115.shared;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import org.apache.log4j.Logger;

public class RegistryHelper {
	
	private final static Logger log = Logger.getLogger(RegistryHelper.class);

	public static synchronized Registry getRegistry() throws PropertiesInvalidException, RemoteException {
		
		RegistryProperties prop = new RegistryProperties();
		
		try {
			// try to get existing registry
			Registry existing = LocateRegistry.getRegistry(prop.getHost(), prop.getPort());
			existing.list(); // force actual connection to registry
			log.info("Registry found");
			return existing;
		} catch (ConnectException e)  {
			log.warn("Existing registry could not be found, creating new one...");
		} catch (RemoteException e) {
			log.warn("Remote exception while getting registry", e);
			throw e;
		}
		
		// if no one found - try to create new one
		return LocateRegistry.createRegistry(prop.getPort());
	}
}
