package at.tuwien.group115.shared;

public class PropertiesInvalidException extends Exception {

	private static final long serialVersionUID = 1L;

	public PropertiesInvalidException() {}

	public PropertiesInvalidException(String msg) {
		super(msg);
	}

	public PropertiesInvalidException(String msg, Throwable e) {
		super(msg, e);
	}

}