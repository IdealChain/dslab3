package at.tuwien.group115.shared;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class RegistryProperties {
	
	private final static Logger log = Logger.getLogger(RegistryProperties.class);
	
	private String host;
	private int port;
	
	public RegistryProperties() throws PropertiesInvalidException {
		
		InputStream is = ClassLoader.getSystemResourceAsStream("registry.properties");
		
		if (is != null) {
			Properties props = new Properties();
			try {
				props.load(is);
				this.host = props.getProperty("registry.host");
				this.port = Integer.parseInt(props.getProperty("registry.port"));
			} catch (IOException e) {
				throw new PropertiesInvalidException("IO error", e);
			} catch (NumberFormatException e) {
				throw new PropertiesInvalidException("Port not valid", e);
			} finally {
				try {
					is.close();
				} catch (IOException e) {}
			}
		} else {
			throw new PropertiesInvalidException("File not found");
		}
		
		log.info(String.format("registry.properties: %s:%d", host, port));
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}

}
