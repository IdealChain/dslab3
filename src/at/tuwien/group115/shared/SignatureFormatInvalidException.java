package at.tuwien.group115.shared;

public class SignatureFormatInvalidException extends Exception {

	private static final long serialVersionUID = 1L;

	public SignatureFormatInvalidException() {
		super();
	}

	public SignatureFormatInvalidException(String msg) {
		super(msg);
	}

}