package at.tuwien.group115.shared;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BidCommand {
	
	private final static Pattern bidPattern = Pattern.compile("^ ?([0-9]+) ([0-9\\.]+)$");
	
	private int auctionID;
	private double price;
	
	public BidCommand(String prefix, String bidMessage) throws BidFormatIllegalException {
		
		if (!bidMessage.startsWith(prefix))
			throw new BidFormatIllegalException("Expected bid message to start with " + prefix);
		
		bidMessage = bidMessage.substring(prefix.length());
		
		Matcher bidMatcher = bidPattern.matcher(bidMessage);
		if (!bidMatcher.matches())
			throw new BidFormatIllegalException("Bid parameter format not valid");
		
		try {
			auctionID = Integer.parseInt(bidMatcher.group(1));
			price = Double.parseDouble(bidMatcher.group(2));
		} catch (NumberFormatException e) {
			throw new BidFormatIllegalException("Number format not valid");
		}
	}

	public BidCommand(int auctionID, double price) {
		this.auctionID = auctionID;
		this.price = price;
	}

	public int getAuctionID() {
		return auctionID;
	}

	public double getPrice() {
		return price;
	}
	
	/**
	 * Opposite of pattern - creates bid parameters.
	 */
	@Override
	public String toString() {
		return String.format(Locale.ENGLISH, "%d %.2f", auctionID, price);
	}
	
	public String getTimestamp(long timestamp) {
		return String.format("!timestamp %s %d", toString(), timestamp);
	}
	
	public String getGetTimestamp() {
		return String.format("!getTimestamp %s", toString());
	}
	
	public String getSignedBid(String signatures) {
		return String.format("!signedBid %s %s", toString(), signatures);
	}
}
