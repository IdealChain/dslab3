package at.tuwien.group115.shared;

import org.bouncycastle.util.encoders.Base64;

public class Base64Helper {
	
	public static String encodeString(String msg) {
		if (msg.isEmpty())
			return "";

		return encodeByte(msg.getBytes());
	}
	
	public static String decodeString(String msg) {
		if (msg.isEmpty())
			return "";
		
		return new String(decodeByte(msg));
	}
	
	public static String encodeByte(byte[] msg) {
		return new String(Base64.encode(msg));
	}
	
	public static byte[] decodeByte(String msg) {
		return Base64.decode(msg.getBytes());
	}
}
