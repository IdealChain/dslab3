package at.tuwien.group115.shared.security;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

import at.tuwien.group115.shared.Base64Helper;

public class SignatureHelper {
	
	private final static String algo = "RSA";

	public static String createSignature(String msg, PrivateKey myKey)
			throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
		
		Signature s = Signature.getInstance(algo);
		s.initSign(myKey);
		s.update(msg.getBytes());
		byte[] sig = s.sign();
		
		return Base64Helper.encodeByte(sig);		
	}
	
	public static boolean verifySignature(String msg, String signature, PublicKey signersKey)
			throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {

		byte[] sig = Base64Helper.decodeByte(signature);
		
		Signature s = Signature.getInstance(algo);
		s.initVerify(signersKey);
		s.update(msg.getBytes());
		return s.verify(sig);
	}
}
