package at.tuwien.group115.shared.security;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

public class RSAChannel implements IChannel<String> {
	
	private static String algo = "RSA/NONE/OAEPWithSHA256AndMGF1Padding";
	
	private IChannel<byte[]> inner;
	private Cipher decCipher;
	private Cipher encCipher = null;
	
	public RSAChannel(IChannel<byte[]> inner, PrivateKey myKey) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		
		this.inner = inner;
		
		decCipher = Cipher.getInstance(algo);
		decCipher.init(Cipher.DECRYPT_MODE, myKey);
	}
	
	public RSAChannel(IChannel<byte[]> inner, PrivateKey myKey, PublicKey theirKey) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		
		this(inner, myKey);
		this.setRemoteKey(theirKey);
	}
	
	public void setRemoteKey(PublicKey theirKey) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		
		encCipher = Cipher.getInstance(algo);
		encCipher.init(Cipher.ENCRYPT_MODE, theirKey);
	}

	@Override
	public void send(String msg) throws IOException {
		if (msg.isEmpty()) {
			inner.send(new byte[0]);
			return;
		}
		
		byte[] encrypted;

		try {
			encrypted = encCipher.doFinal(msg.getBytes());
			//log.debug(String.format("RSA encrypt %s: %.10s... [%d]", msg, new BigInteger(1, encrypted).toString(16), encrypted.length));
		} catch (Exception e) {
			throw new IOException("RSA encryption", e);
		}
		
		inner.send(encrypted);
	}

	@Override
	public String receive() throws IOException {
		byte[] encrypted = inner.receive();
		
		if (encrypted == null)
			return null;
		
		if (encrypted.length == 0)
			return "";
		
		try {
			//log.debug(String.format("RSA decrypting %.10s... [%d]", new BigInteger(1, encrypted).toString(16), encrypted.length));
			return new String(decCipher.doFinal(encrypted));
			
		} catch (Exception e) {
			throw new IOException("RSA decryption", e);
		}
	}

	@Override
	public void close() throws IOException {
		inner.close();
	}

}
