package at.tuwien.group115.shared.security;

import java.io.IOException;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Special channel that just queues sended messages and delivers them right back on receive.
 */
public class LoopbackChannel implements IChannel<String> {
	
	protected LinkedList<String> pendingMessages = new LinkedList<String>();
	
	public LoopbackChannel() {};

	@Override
	public void send(String msg) throws IOException {
		pendingMessages.add(msg);
	}

	@Override
	public String receive() throws IOException {
		try {
			return pendingMessages.pop();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	@Override
	public void close() throws IOException {}

}
