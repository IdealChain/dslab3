package at.tuwien.group115.shared.security;

public class KeyReaderException extends Exception {

	private static final long serialVersionUID = 1L;

	public KeyReaderException() {}

	public KeyReaderException(String msg) {
		super(msg);
	}
	
	public KeyReaderException(String msg, Throwable e) {
		super(msg, e);
	}

}