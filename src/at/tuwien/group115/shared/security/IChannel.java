package at.tuwien.group115.shared.security;

import java.io.IOException;

/**
 * Interface for a communication channel, which allows sending and receiving of messages.
 * 
 * @param <T> The type this channel supports for sending and receiving.
 */
public interface IChannel<T> {
	
	/**
	 * Sends a message over this channel.
	 * @param msg The message.
	 * @throws IOException
	 */
	void send(T msg) throws IOException;
	
	/**
	 * Receives a message over this channel (blocking).
	 * Take care to pass-through null values unaltered.
	 * 
	 * @return The received message, or null in case the underlying channel is defunct.
	 * @throws IOException
	 */
	T receive() throws IOException;
	
	/**
	 * Closes this channel and all underlying channels.
	 * @throws IOException
	 */
	void close() throws IOException;

}
