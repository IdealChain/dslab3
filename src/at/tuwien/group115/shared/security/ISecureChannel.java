package at.tuwien.group115.shared.security;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import javax.crypto.NoSuchPaddingException;

public interface ISecureChannel extends IChannel<String> {

	/**
	 * Initializes the underlying secure channel from the client's perspective.
	 * @param cred The client's credentials.
	 * @return Success or not.
	 */
	boolean login(ClientCredentials cred)
		throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException;

	/**
	 * Initializes the underlying secure channel from the server's perspective.
	 * @param msg The first message, which you suspect to be a client handshake first message.
	 * @param myKey Private key of the server.
	 * @param auth Client authenticator interface.
	 * @return Success or not.
	 */
	boolean initServer(String msg, PrivateKey myKey, IAuthenticateClient auth)
		throws InvalidKeyException,	NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException;

	/**
	 * Resets the underlying channel into unencrypted, unauthenticated mode.
	 */
	void reset();

	/**
	 * Gets whether the underlying secure channel is currently in secure mode.
	 */
	boolean isSecure();
	
	/**
	 * Gets the username of the currently logged in user (only in secure mode).
	 */
	String getUsername();
}