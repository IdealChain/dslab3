package at.tuwien.group115.shared.security;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;

public class Generators {
	
	/**
	 * Generates and returns a secure random number.
	 * @param l The desired length, in bytes.
	 * @return A secure random number byte array.
	 */
	public static byte[] generateSecureRandomNumber(int l) {
		// generates a 32 byte secure random number 
		SecureRandom secureRandom = new SecureRandom(); 
		final byte[] number = new byte[l]; 
		secureRandom.nextBytes(number);
		return number;
	}
	
	/**
	 * Generates and returns a new AES key.
	 * @param b The desired length, in bits.
	 * @return A random AES key.
	 * @throws NoSuchAlgorithmException
	 */
	public static SecretKey generateAESKey(int b) throws NoSuchAlgorithmException {
		KeyGenerator generator = KeyGenerator.getInstance("AES"); 
		// KEYSIZE is in bits
		generator.init(b); 
		return generator.generateKey();
	}

	/**
	 * Generates and returns a new MAC key 
	 * (using the HmacSHA256 algorithm).
	 * @param bytes The bytes to generate a key of.
	 * @param secretKey The secret key.
	 * @return The generated key.
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException 
	 */
	public static byte[] generateHMAC(byte[] bytes, Key secretKey) throws NoSuchAlgorithmException, InvalidKeyException
	{
		Mac hMac = Mac.getInstance("HmacSHA256");
		hMac.init(secretKey);
		hMac.update(bytes);
		return hMac.doFinal(); 
	}
}
