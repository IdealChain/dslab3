package at.tuwien.group115.shared.security;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Associated data for a client login / handshake.
 */
public class ClientCredentials {
	
	String username;
	int clientPort;
	PrivateKey myKey;
	PublicKey serverKey;
	
	/**
	 * @param username User name of the client.
	 * @param clientPort Port where the client is reachable.
	 * @param myKey Private key of the client to login.
	 * @param serverKey Public key of the server.
	 */
	public ClientCredentials(String username, int clientPort, PrivateKey myKey, PublicKey serverKey) {
		this.username = username;
		this.clientPort = clientPort;
		this.myKey = myKey;
		this.serverKey = serverKey;
	}

	public String getUsername() {
		return username;
	}

	public int getClientPort() {
		return clientPort;
	}

	public PrivateKey getMyKey() {
		return myKey;
	}

	public PublicKey getServerKey() {
		return serverKey;
	}

}
