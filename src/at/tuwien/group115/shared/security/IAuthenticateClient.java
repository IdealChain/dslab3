package at.tuwien.group115.shared.security;

import java.security.PublicKey;

/**
 * Callback interface to authenticate a client user and get his public key.
 */
public interface IAuthenticateClient {
	PublicKey getPublicKey(String username);
	void clientAuthenticated(String username, int clientPort);
}
