package at.tuwien.group115.shared.security;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.Key;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.util.encoders.Hex;

public class KeyReader {
	
	private final static Logger log = Logger.getLogger(KeyReader.class);
	
	private static Pattern clientKeyPattern = Pattern.compile("^([a-zA-Z0-9_\\-]+)\\.pub\\.pem$");
	
	public static Key readSecretKey(String path) throws FileNotFoundException, KeyReaderException {
		byte[] keyBytes = new byte[1024];
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(path);
			fis.read(keyBytes);
		} catch (IOException e) {
			throw new KeyReaderException("Secret key reading", e);
		} finally {
				try {
					if (fis != null)
						fis.close();
				} catch (IOException e) {
					log.warn(e);
				}
		}
		byte[] input = Hex.decode(keyBytes);

		return new SecretKeySpec(input, "HmacSHA256"); 
	}
	
	public static Key generateSecretKey(String path) throws KeyReaderException, FileNotFoundException {
		KeyGenerator keyGen;
		try {
			keyGen = KeyGenerator.getInstance("HmacSHA256");
			keyGen.init(4096);
		} catch (NoSuchAlgorithmException e) {
			throw new KeyReaderException("No generation algorithm", e);
		}
		SecretKey key = keyGen.generateKey();
		byte[] keyBytes = Hex.encode(key.getEncoded());
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(path);
			fos.write(keyBytes);
			fos.write('\n');
		} catch (IOException e) {
			throw new KeyReaderException("Secret key generating", e);
		} finally {
			try {
				if (fos != null)
					fos.close();
			} catch (IOException e) {
				log.warn(e);
			}
		}
		
		return readSecretKey(path);
	}
	
	public static PrivateKey readPrivateKey(String path) throws KeyReaderException, FileNotFoundException {
		try {
			PEMReader in = null;
			try {
				in = new PEMReader(new FileReader(path), new PasswordFinder() { 
					@Override 
					public char[] getPassword() { 
						// reads the password from standard input for decrypting the private key
						System.out.println("Enter passphrase for private key:");
						try {
							return new BufferedReader(new InputStreamReader(System.in)).readLine().trim().toCharArray();
						} catch (IOException e) {
							log.error("IOException during passphrase reading", e);
							return "".toCharArray();
						} 
					} 
				}); 
		
				log.info("Reading private key from " + path);
				KeyPair keyPair = (KeyPair) in.readObject();
				return keyPair.getPrivate();
				
			} finally {
				if (in != null)
					in.close();
			}
		} catch (FileNotFoundException e) {
			throw e;
		} catch(IOException e) {
			throw new KeyReaderException("Private key reading", e);
		}
		
	}
	
	public static PublicKey readPublicKey(String path) throws KeyReaderException, FileNotFoundException {
		try {
			PEMReader in = null;
			try {
				in = new PEMReader(new FileReader(path));
				log.info("Reading public key from " + path);
				return (PublicKey) in.readObject();
				
			} finally {
				if (in != null)
					in.close();
			}
		} catch (FileNotFoundException e) {
			throw e;
		} catch(IOException e) {
			throw new KeyReaderException("Public key reading", e);
		}
	}
	
	public static Map<String, PublicKey> readPublicKeys(String dir) {
		HashMap<String, PublicKey> map = new HashMap<String, PublicKey>();
		
		log.info(String.format("Reading existing public keys %s...", dir));
		for (File f : new File(dir).listFiles()) {
			
			if (!f.isFile())
				continue;
			
			Matcher m = clientKeyPattern.matcher(f.getName());
			if (!m.matches())
				continue;
			
			try {
				String username = m.group(1);
				PublicKey userkey = KeyReader.readPublicKey(f.getPath());
				
				map.put(username, userkey);
				
			} catch (FileNotFoundException e) {
				log.warn(String.format("Skipping non existing keyfile %s", f.getName()));
			}  catch (KeyReaderException e) {
				log.warn(String.format("Skipping unreadable keyfile %s: %s", f.getName(), e.getCause().getMessage()));
			}
		}
		
		return map;
	}

}
