package at.tuwien.group115.shared.security;

import java.io.IOException;
import at.tuwien.group115.shared.Base64Helper;

public class Base64Channel implements IChannel<byte[]> {
	
	private IChannel<String> inner;
	
	public Base64Channel(IChannel<String> inner) {
		this.inner = inner;
	}

	@Override
	public void send(byte[] msg) throws IOException {
		
		if (msg.length == 0)
			inner.send("");
		else
			inner.send(Base64Helper.encodeByte(msg));
	}

	@Override
	public byte[] receive() throws IOException {
		String enc = inner.receive();
		
		if (enc == null)
			return null;
		
		if (enc.isEmpty())
			return new byte[0];
				
		return Base64Helper.decodeByte(enc);		
	}

	@Override
	public void close() throws IOException {
		inner.close();
	}

}
