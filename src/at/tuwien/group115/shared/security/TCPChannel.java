package at.tuwien.group115.shared.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.log4j.Logger;

public class TCPChannel implements IChannel<String> {

	private final static Logger log = Logger.getLogger(TCPChannel.class);
	
	Socket socket;
	PrintWriter out;
	BufferedReader in;
	
	public TCPChannel(Socket socket) throws IOException {
		this.socket = socket;
		out = new PrintWriter(socket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		log.info(String.format("TCPChannel to %s", socket.getRemoteSocketAddress().toString()));
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	@Override
	public void send(String msg) throws IOException {
		
		//log.debug(msg.isEmpty() ? "TCPChannel send: -" : String.format("TCPChannel send: %.30s(..)", msg));
		out.println(msg);
	}

	@Override
	public String receive() throws IOException {
		String msg = in.readLine();
		if (msg != null) {
			//log.debug(msg.isEmpty() ? "TCPChannel recv: -" : String.format("TCPChannel recv: %.30s(..)", msg));
		}
		return msg;
	}

	@Override
	public void close() throws IOException {
		if (socket != null)
			socket.close();
	}

}
