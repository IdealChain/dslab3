package at.tuwien.group115.shared.security;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AESChannel implements IChannel<String> {
	
	private static String algo = "AES/CTR/NoPadding";
	
	private IChannel<byte[]> inner;
	private Cipher encCipher;
	private Cipher decCipher;
	
	public AESChannel(IChannel<byte[]> inner, byte[] secretKey, byte[] iv) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		
		this.inner = inner;
		SecretKeySpec keySpec = new SecretKeySpec(secretKey, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(iv);
		
		encCipher = Cipher.getInstance(algo);
		encCipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
		
		decCipher = Cipher.getInstance(algo);
		decCipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
		
	}

	@Override
	public void send(String msg) throws IOException {
		if (msg == null || msg.isEmpty()) {
			inner.send(new byte[0]);
			return;
		}
		
		byte[] encrypted;

		try {
			encrypted = encCipher.doFinal(msg.getBytes());
			//log.debug(String.format("AES encrypt %s: %.10s... [%d]", msg, new BigInteger(1, encrypted).toString(16), encrypted.length));
		} catch (Exception e) {
			throw new IOException("AES encryption", e);
		}
		
		inner.send(encrypted);
	}

	@Override
	public String receive() throws IOException {
		byte[] encrypted = inner.receive();
		
		if (encrypted == null)
			return null;
		
		if (encrypted.length == 0)
			return "";
		
		try {			
			//log.debug(String.format("AES decrypting %.10s... [%d]", new BigInteger(1, encrypted).toString(16), encrypted.length));
			return new String(decCipher.doFinal(encrypted));
			
		} catch (Exception e) {
			throw new IOException("AES decryption", e);
		}
	}

	@Override
	public void close() throws IOException {
		inner.close();
	}

}
