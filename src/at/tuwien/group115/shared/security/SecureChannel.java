package at.tuwien.group115.shared.security;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;

import at.tuwien.group115.shared.Base64Helper;

/**
 * Provides a secure channel for communication, which uses an RSA-encrypted
 * handshake first to ensure the communication partner's identity, and afterward
 * switches to AES encryption with a random session key.
 * 
 * Channel stays unensecure (unencrypted, unauthenticated) until the client initializes
 * a secure session successfully. Check with isSecure().
 */
public class SecureChannel implements ISecureChannel {
	
	private final static Logger log = Logger.getLogger(SecureChannel.class);

	private IChannel<String> rawChannel;
	private IChannel<String> currentChannel;
	private boolean server;
	private String username = null;
	
	/**
	 * Handshake message parsing patterns.
	 */
	final static String B64 = "a-zA-Z0-9/+";
	final static Pattern firstMessagePattern = Pattern.compile("!login ([a-zA-Z0-9_\\-]+) ([0-9]+) (["+B64+"]{43}=)");
	final static Pattern secondMessagePattern = Pattern.compile("!ok (["+B64+"]{43}=) (["+B64+"]{43}=) (["+B64+"]{43}=) (["+B64+"]{22}==)");
	final static Pattern thirdMessagePattern = Pattern.compile("(["+B64+"]{43}=)");
	
	public SecureChannel(IChannel<String> inner, boolean server) {
		this.currentChannel = this.rawChannel = inner;
		this.server = server;
	}

	public boolean login(ClientCredentials cred) 
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		
		assert !this.isServer() : "login only for client";
		assert !this.isSecure() : "channel already secure";
		
		RSAChannel rsa = new RSAChannel(new Base64Channel(rawChannel), cred.getMyKey(), cred.getServerKey());
		String clientChallenge = Base64Helper.encodeByte(Generators.generateSecureRandomNumber(32));
		
		try {
			String firstMessage = String.format("!login %s %d %s", cred.getUsername(), cred.getClientPort(), clientChallenge);
			rsa.send(firstMessage);
			
			Matcher secondMessage = secondMessagePattern.matcher(rsa.receive());
			assert secondMessage.matches() : "2nd message illegal";

			String returnedClientChallenge = secondMessage.group(1);
			String serverChallenge = secondMessage.group(2);
			byte[] sessionKey = Base64Helper.decodeByte(secondMessage.group(3));
			byte[] iv = Base64Helper.decodeByte(secondMessage.group(4));
			assert returnedClientChallenge.equals(clientChallenge) : "wrong client challenge returned";
			
			// handshake already successful from the clients perspective
			currentChannel = new AESChannel(new Base64Channel(rawChannel), sessionKey, iv);
			currentChannel.send(serverChallenge);
			
			this.username = cred.getUsername();
			log.info("Handshake successful");
			
		} catch (NullPointerException e) {
			log.error("Handshake not successful", e);
			reset();
		} catch (IOException e) {
			log.error("Handshake not successful", e);
			reset();
		}
		
		return this.isSecure();
	}
	
	public boolean initServer(String msg, PrivateKey myKey, IAuthenticateClient auth)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		
		assert this.isServer() : "initServer only for server";
		assert !this.isSecure() : "channel already secure";
		
		RSAChannel rsa = null;
		AESChannel aes = null;
		
		Matcher firstMessage = null;
		
		try {
			// construct loopback RSA channel to inject still encrypted message into channel hirarchy for decryption
			LoopbackChannel loop = new LoopbackChannel();
			loop.send(msg);
			rsa = new RSAChannel(new Base64Channel(loop), myKey);
			
			firstMessage = firstMessagePattern.matcher(rsa.receive());
			assert firstMessage.matches() : "1st message not a handshake init or illegal";
			
		} catch (IOException e) {
			log.info("Tried RSA decryption unsuccessfully");
		}
		
		if (firstMessage == null)
			return false;
		
		try {
			String username = firstMessage.group(1);
			int clientPort = Integer.parseInt(firstMessage.group(2));
			String clientChallenge = firstMessage.group(3);
			
			// check if user is valid and allowed to login
			PublicKey clientKey = auth.getPublicKey(username);
			if (clientKey == null) 
				return false;
			rsa = new RSAChannel(new Base64Channel(rawChannel), myKey, clientKey);
			
			// generate aes key, iv and server challenge
			byte[] sessionKey = Generators.generateAESKey(256).getEncoded();
			byte[] iv = Generators.generateSecureRandomNumber(16);
			String serverChallenge = Base64Helper.encodeByte(Generators.generateSecureRandomNumber(32));
			
			String secondMessage = String.format("!ok %s %s %s %s",
					clientChallenge, serverChallenge, Base64Helper.encodeByte(sessionKey), Base64Helper.encodeByte(iv));
			rsa.send(secondMessage);
			
			// create aes channel and receive third message
			aes = new AESChannel(new Base64Channel(rawChannel), sessionKey, iv);
			Matcher thirdMessage = thirdMessagePattern.matcher(aes.receive());
			assert thirdMessage.matches() && thirdMessage.group(1).equals(serverChallenge) : "wrong server challenge returned";
			
			// handshake successful, we are good to go
			auth.clientAuthenticated(username, clientPort);
			currentChannel = aes;
			log.info("Handshake successful");
			
		} catch (NullPointerException e) {
			log.error("Handshake not successful", e);
			reset();
		} catch (IOException e) {
			log.error("Handshake not successful", e);
			reset();
		} catch (NumberFormatException e) {
			log.error("Client port was not a number");
			reset();
		}
		
		return this.isSecure();
	}
	
	public void reset() {
		username = null;
		currentChannel = rawChannel;
	}
	
	public boolean isSecure() {
		return currentChannel instanceof AESChannel;
	}
	
	public boolean isServer() {
		return this.server;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	@Override
	public void send(String msg) throws IOException {
		
		// base64 encode server's response in order to escape newline characters
		if (!this.isSecure() && this.isServer())
			msg = Base64Helper.encodeString(msg);
		
		currentChannel.send(msg);
	}

	@Override
	public String receive() throws IOException {
		String msg = currentChannel.receive();
		
		// base64 decode server's response (encoded because it may contain newlines)
		try {
			if (!this.isSecure() && !this.isServer() && msg != null)
				msg = Base64Helper.decodeString(msg);
		} catch (Exception e) {
			log.warn("Base64 response expected", e);
		}
		
		return msg;
	}

	@Override
	public void close() throws IOException {
		currentChannel.close();
	}
}
