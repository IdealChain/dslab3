package at.tuwien.group115.shared;

public class NotLoggedInException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotLoggedInException() { }

	public NotLoggedInException(String msg) {
		super(msg);
	}

}
