package at.tuwien.group115.shared;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Signature {
	
	private final static String B64 = "a-zA-Z0-9/+";
	private final static Pattern signaturePattern = Pattern.compile("^([a-zA-Z0-9_\\-]+):([0-9]+):(["+B64+"]+=+)$");
	
	private String username;
	private long timestamp;
	private String signature;
	
	public Signature(String sig) throws SignatureFormatInvalidException {
		Matcher m = signaturePattern.matcher(sig);
		if (!m.matches()) 
			throw new SignatureFormatInvalidException("Signature format not valid");
		
		try {
			this.username = m.group(1);
			this.timestamp = Long.parseLong(m.group(2));
			this.signature = m.group(3);
			
		} catch (NumberFormatException e) {
			throw new SignatureFormatInvalidException("Number format not valid");
		}
	}
	
	public Signature(String username, String timestamp, String signature) {
		this(username, Long.parseLong(timestamp), signature);
	}
	
	public Signature(String username, long timestamp, String signature) {
		this.username = username;
		this.timestamp = timestamp;
		this.signature = signature;
	}
	
	public String getUsername() {
		return username;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public String getSignature() {
		return signature;
	}

	/**
	 * Opposite of pattern - creates signature string.
	 */
	@Override
	public String toString() {
		return String.format("%s:%d:%s", username, timestamp, signature);
	}
}
