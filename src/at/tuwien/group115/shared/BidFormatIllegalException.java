package at.tuwien.group115.shared;

public class BidFormatIllegalException extends Exception {

	private static final long serialVersionUID = 1L;

	public BidFormatIllegalException() {
		super();
	}

	public BidFormatIllegalException(String msg) {
		super(msg);
	}

}