package at.tuwien.group115.analytics;

import java.rmi.Remote;
import java.rmi.RemoteException;

import at.tuwien.group115.events.Event;

public interface IAnalyticsEventCallback extends Remote 
{	
	void processEvent(Event event) throws RemoteException;
}
