package at.tuwien.group115.analytics.aggregators;

import at.tuwien.group115.events.EventTypeInvalidException;
import at.tuwien.group115.events.StatisticsEvent;

public abstract class SubAggregator<T> {

	private Aggregator parent;
	
	protected SubAggregator(Aggregator parent) {
		this.parent = parent;
	}
	
	protected abstract void processEvent(T event) throws EventTypeInvalidException;
	
	protected void publishEvent(StatisticsEvent event) {
		parent.publishEvent(event);
	}	
}
