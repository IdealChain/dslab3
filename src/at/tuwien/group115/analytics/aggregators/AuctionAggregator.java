package at.tuwien.group115.analytics.aggregators;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import at.tuwien.group115.analytics.StatisticsEventGenerator;
import at.tuwien.group115.events.EventType;
import at.tuwien.group115.events.AuctionEvent;
import at.tuwien.group115.events.EventTypeInvalidException;

public class AuctionAggregator extends SubAggregator<AuctionEvent> {

	private final static Logger log = Logger.getLogger(AuctionAggregator.class);
	
	private double auctionTimeSum = 0;
	private int auctionCount = 0;
	private int successfulAuctionCount = 0;
	
	private class RunningAuction {
		long start;
		boolean success = false;
		
		public RunningAuction(AuctionEvent start) {
			this.start = start.timestamp;
		}
		
		void setSuccess() {
			this.success = true;
		}
	}
	
	private Hashtable<Long, RunningAuction> runningAuctions = new Hashtable<Long, RunningAuction>();
	
	protected AuctionAggregator(Aggregator parent) {
		super(parent);
	}
	
	@Override
	protected synchronized void processEvent(AuctionEvent event) throws EventTypeInvalidException {
		
		if (event.getEventType() == EventType.AUCTION_STARTED) {
			
			runningAuctions.put(event.auctionId, new RunningAuction(event));
			
		} else if (event.getEventType() == EventType.AUCTION_ENDED) {
			
			RunningAuction auction = runningAuctions.remove(event.auctionId);
			
			if (auction == null) {
				log.warn(String.format("Ending auction %d: no start event seen", event.auctionId));
				return;
			}
			
			double auctionTime = (event.timestamp - auction.start) / 1000.0;
			log.info(String.format("Auction %d ran for %f seconds (success: %b)", 
					event.auctionId, auctionTime, auction.success));
			addAuction(auctionTime, auction.success);
		}
	}
	
	public synchronized void markAuctionSuccessful(long id) {
		
		RunningAuction auction = runningAuctions.get(id);
		
		if (auction == null) {
			log.warn(String.format("Cannot mark auction %d as successful: no start event seen", id));
			return;
		}
		
		auction.setSuccess();
	}
	
	private void addAuction(double auctionTime, boolean successful) {
		
		auctionTimeSum += auctionTime;
		auctionCount++;
		
		if (successful)
			successfulAuctionCount++;
		
		publishEvent(StatisticsEventGenerator.createEvent(
				getAuctionTimeAvg(), 
				EventType.AUCTION_TIME_AVG));
		
		publishEvent(StatisticsEventGenerator.createEvent(
				getAuctionSuccessRatio(), 
				EventType.AUCTION_SUCCESS_RATIO));
	}

	private double getAuctionTimeAvg() {
		return auctionTimeSum / auctionCount;
	}
	
	private double getAuctionSuccessRatio() {
		return successfulAuctionCount * 1.0 / auctionCount;
	}
}
