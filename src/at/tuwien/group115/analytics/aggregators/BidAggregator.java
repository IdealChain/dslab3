package at.tuwien.group115.analytics.aggregators;

import java.util.Date;

import at.tuwien.group115.analytics.StatisticsEventGenerator;
import at.tuwien.group115.events.BidEvent;
import at.tuwien.group115.events.EventType;
import at.tuwien.group115.events.EventTypeInvalidException;

public class BidAggregator extends SubAggregator<BidEvent> {

	private AuctionAggregator auctionAggregator;
	
	private Date startTimestamp;
	private int bidCount = 0;
	private double bidMaxAmount = 0;
	
	protected BidAggregator(Aggregator parent, AuctionAggregator auctionAggregator) {
		super(parent);
		startTimestamp = new Date();
		this.auctionAggregator = auctionAggregator;
	}
	
	@Override
	protected synchronized void processEvent(BidEvent event) throws EventTypeInvalidException {
		
		if (event.getEventType() == EventType.BID_PLACED) {
			setBidCount(getBidCount() + 1);
			
			if (event.price > getBidMaxAmount())
				setBidMaxAmount(event.price);
			
			auctionAggregator.markAuctionSuccessful(event.auctionId);
		}
	}
	
	private int getBidCount() {
		return bidCount;
	}

	private void setBidCount(int value) {
		if (bidCount == value)
			return;
		
		bidCount = value;
		publishEvent(StatisticsEventGenerator.createEvent(
				getBidsPerMinute(), 
				EventType.BID_COUNT_PER_MINUTE));
	}

	private double getBidMaxAmount() {
		return bidMaxAmount;
	}

	private void setBidMaxAmount(double value) {
		if (bidMaxAmount == value)
			return;
		
		bidMaxAmount = value;
		publishEvent(StatisticsEventGenerator.createEvent(
				bidMaxAmount, 
				EventType.BID_PRICE_MAX));
	}

	private double getElapsedMinutes() {
		long elapsedMS = new Date().getTime() - startTimestamp.getTime();
		return elapsedMS / 60000.0;
	}
	
	private double getBidsPerMinute() {
		return this.bidCount / this.getElapsedMinutes();
	}
}
