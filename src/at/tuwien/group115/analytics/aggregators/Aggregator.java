package at.tuwien.group115.analytics.aggregators;

import java.rmi.RemoteException;
import org.apache.log4j.Logger;

import at.tuwien.group115.analytics.IAnalyticsEventCallback;
import at.tuwien.group115.events.*;

public class Aggregator {

	private final static Logger log = Logger.getLogger(Aggregator.class);
	private IAnalyticsEventCallback callback;
	
	private AuctionAggregator auctionAggregator = new AuctionAggregator(this);
	private UserAggregator userAggregator = new UserAggregator(this);
	private BidAggregator bidAggregator = new BidAggregator(this, auctionAggregator);

	public void setCallback(IAnalyticsEventCallback callback) {
		this.callback = callback;
	}
	
	public void processEvent(Event event) {
		
		try {
			if (event instanceof AuctionEvent) {
				auctionAggregator.processEvent((AuctionEvent)event);
			} else if (event instanceof UserEvent) {
				userAggregator.processEvent((UserEvent)event);
			} else if (event instanceof BidEvent) {
				bidAggregator.processEvent((BidEvent)event);
			}
			
		} catch (EventTypeInvalidException e) {
			log.warn("Event type unknown: " + event.type);
		}
	}
	
	protected void publishEvent(StatisticsEvent event) {
		
		if (this.callback == null) {
			log.fatal("No callback set to publish generated events");
			return;
		}
		
		try {
			log.info("Generated " + event.toString());
			this.callback.processEvent(event);
		} catch (RemoteException e) {
			log.warn("Remote error during event pushing", e);
		}
	}	
	
}
