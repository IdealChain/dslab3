package at.tuwien.group115.analytics.aggregators;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import at.tuwien.group115.analytics.StatisticsEventGenerator;
import at.tuwien.group115.events.EventType;
import at.tuwien.group115.events.EventTypeInvalidException;
import at.tuwien.group115.events.UserEvent;

public class UserAggregator extends SubAggregator<UserEvent> {

	private final static Logger log = Logger.getLogger(UserAggregator.class);
	
	private double sessionTimeMin = Double.MAX_VALUE;
	private double sessionTimeMax = 0;
	
	private double sessionTimeSum = 0;
	private int sessionCount = 0;
	
	private Hashtable<String, UserEvent> runningSessions = new Hashtable<String, UserEvent>();
	
	protected UserAggregator(Aggregator parent) {
		super(parent);
	}
	
	@Override
	protected synchronized void processEvent(UserEvent event) throws EventTypeInvalidException {
		
		if (event.getEventType() == EventType.USER_LOGIN) {
			runningSessions.put(event.userName, event);
			
		} else if (event.getEventType() == EventType.USER_LOGOUT ||
				event.getEventType() == EventType.USER_DISCONNECTED) {
			
			UserEvent login = runningSessions.remove(event.userName);
			
			if (login == null) {
				log.warn(String.format("Outlogging user %s: no login event seen", event.userName));
				return;
			}
			
			double sessionTime = (event.timestamp - login.timestamp) / 1000.0;
			log.info(String.format("User %s was logged in for %f seconds", event.userName, sessionTime));
			addSessionTime(sessionTime);
		}
	}
	
	private void addSessionTime(double sessionTime) {
		
		if (sessionTime < getSessionTimeMin())
			setSessionTimeMin(sessionTime);
		if (sessionTime > getSessionTimeMax())
			setSessionTimeMax(sessionTime);
		
		sessionTimeSum += sessionTime;
		sessionCount++;
		
		publishEvent(StatisticsEventGenerator.createEvent(
				getSessionTimeAvg(), 
				EventType.USER_SESSIONTIME_AVG));
	}

	private double getSessionTimeAvg() {
		return sessionTimeSum / sessionCount;
	}	
	
	private double getSessionTimeMin() {
		return sessionTimeMin;
	}

	private void setSessionTimeMin(double value) {
		if (sessionTimeMin == value)
			return;
		
		sessionTimeMin = value;
		publishEvent(StatisticsEventGenerator.createEvent(
				sessionTimeMin, 
				EventType.USER_SESSIONTIME_MIN));
	}
	
	private double getSessionTimeMax() {
		return sessionTimeMax;
	}

	private void setSessionTimeMax(double value) {
		if (sessionTimeMax == value)
			return;
		
		sessionTimeMax = value;
		publishEvent(StatisticsEventGenerator.createEvent(
				sessionTimeMax, 
				EventType.USER_SESSIONTIME_MAX));
	}	
}
