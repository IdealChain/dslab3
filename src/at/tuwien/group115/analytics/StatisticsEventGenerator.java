package at.tuwien.group115.analytics;

import java.util.Date;

import at.tuwien.group115.events.EventType;
import at.tuwien.group115.events.StatisticsEvent;

public class StatisticsEventGenerator {
	
	private static int nextID = 1;
	
	private synchronized static StatisticsEvent initEvent(StatisticsEvent e, EventType t) {
		e.id = String.format("analytics-event:%d", nextID++);
		e.type = t.toString();
		e.timestamp = new Date().getTime();
		return e;
	}
	
	public static StatisticsEvent createEvent(double value, EventType t) {
		StatisticsEvent e = new StatisticsEvent();
		e.value = value;
		return initEvent(e, t);
	}
}
