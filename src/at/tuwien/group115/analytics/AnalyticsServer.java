package at.tuwien.group115.analytics;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import at.tuwien.group115.analytics.aggregators.Aggregator;
import at.tuwien.group115.events.Event;

public class AnalyticsServer extends UnicastRemoteObject implements IAnalyticsServer {
	
	private final static Logger log = Logger.getLogger(AnalyticsServer.class);
	private static final long serialVersionUID = 1L;
	
	private int nextSubscriptionID = 1;
	
	private Hashtable<String, IAnalyticsEventCallback> callbacks = new Hashtable<String, IAnalyticsEventCallback>();
	private Hashtable<String, Pattern> filters = new Hashtable<String, Pattern>();
	private Aggregator aggregator;

	protected AnalyticsServer() throws RemoteException {
		super();
	}
	
	protected AnalyticsServer(Aggregator aggregator) throws RemoteException {
		super();
		this.aggregator = aggregator;
		this.aggregator.setCallback(this);
	}
	
	public synchronized String createSubscriptionID() {
		
		int id = this.nextSubscriptionID++;
		return String.format("%d", id);
	}
	
	@Override
	public synchronized String subscribe(IAnalyticsEventCallback client, String filterRegex) throws RemoteException 
	{
		String id = this.createSubscriptionID();
		
		callbacks.put(id, client);
		filters.put(id, Pattern.compile(filterRegex));
		
		log.info(String.format("Subscriber %s subscribed (filter %s)", id, filterRegex));
		return id;
	}

	@Override
	public synchronized boolean unsubscribe(String subscriptionID) throws RemoteException 
	{
		IAnalyticsEventCallback client = callbacks.remove(subscriptionID);
		filters.remove(subscriptionID);
		
		if (client == null) {
			log.warn(String.format("Subscriber %s can't be removed - not subscribed", subscriptionID));
			return false;
		}
		
		log.info(String.format("Subscriber %s removed", subscriptionID));
		return true;
	}
	
	@Override
	public void processEvent(Event event) throws RemoteException
	{
		synchronized(this) {
			log.info(String.format("%s received (forwarding to %d subscribers)", 
					event.toString(), callbacks.size()));
			
			ArrayList<String> faultySubscribers = new ArrayList<String>();
			
			// forward event to every subscriber with a matching filter
			Enumeration<String> ids = callbacks.keys();
			while (ids.hasMoreElements()) {
				
				String id = ids.nextElement();
				IAnalyticsEventCallback subscriber = callbacks.get(id);
				Pattern filter = filters.get(id);
				
				if (subscriber == null)
					break;
				
				if (filter != null) {
					Matcher m = filter.matcher(event.type);
					if (!m.matches())
						break;
				}
					
				try {
					subscriber.processEvent(event);
				} catch (RemoteException e) {
					log.warn("Exception during processEvent callback for subscriber " + id, e);
					faultySubscribers.add(id);
				}
			}
			
			for(String id : faultySubscribers) {
				log.info("Removing faulty subscriber " + id);
				this.unsubscribe(id);
			}
		}
		
		// forward event also to aggregator, which in turn might generate new events
		if (aggregator != null)
			aggregator.processEvent(event);
	}
}
