package at.tuwien.group115.analytics;

import java.rmi.Remote;
import java.rmi.RemoteException;

import at.tuwien.group115.events.Event;

public interface IAnalyticsServer extends Remote, IAnalyticsEventCallback
{	
	String subscribe(IAnalyticsEventCallback client, String filterRegex) throws RemoteException;
	boolean unsubscribe(String subscriptionID) throws RemoteException;
	void processEvent(Event event) throws RemoteException;
}