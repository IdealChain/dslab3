package at.tuwien.group115.events;

public class StatisticsEvent extends Event
{
	private static final long serialVersionUID = 1L;
	
	public double value;
	
	@Override
	public String toString() {
		return String.format("%s (%f)", super.toString(), value);
	}
	
	@Override
	protected String getExplanation() throws EventTypeInvalidException {
		switch(this.getEventType()) {
			case AUCTION_SUCCESS_RATIO:
				return String.format("current auction success ratio is %.2f", value);
			case AUCTION_TIME_AVG:
				return String.format("current auction time average is %.2f seconds", value);
			case BID_COUNT_PER_MINUTE:
				return String.format("current bids per minute is %.2f", value);
			case BID_PRICE_MAX:
				return String.format("maximum bid price seen so far is %.2f", value);
			case USER_SESSIONTIME_AVG:
				return String.format("current user session time average is %.2f seconds", value);
			case USER_SESSIONTIME_MAX:
				return String.format("current user session time maximum is %.2f seconds", value);
			case USER_SESSIONTIME_MIN:
				return String.format("current user session time minimum is %.2f seconds", value);
			default:
				throw new EventTypeInvalidException();				
		}		
	}
}
