package at.tuwien.group115.events;

public class BidEvent extends Event
{
	private static final long serialVersionUID = 1L;
	
	public long auctionId;
	public String userName;
	public double price;
	
	@Override
	public String toString() {
		return String.format("%s (%d/%s/%f)", 
				super.toString(), auctionId, userName, price);
	}
	
	@Override
	protected String getExplanation() throws EventTypeInvalidException {
		switch(this.getEventType()) {
			case BID_PLACED:
				return String.format("user %s placed bid %.2f on auction %d", userName, price, auctionId);
			case BID_OVERBID:
				return String.format("user %s was overbid on auction %d", userName, auctionId);
			case BID_WON:
				return String.format("user %s won auction %d with %.2f", userName, auctionId, price);
			default:
				throw new EventTypeInvalidException();	
		}		
	}
}
