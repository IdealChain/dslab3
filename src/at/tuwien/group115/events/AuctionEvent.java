package at.tuwien.group115.events;

public class AuctionEvent extends Event
{
	private static final long serialVersionUID = 1L;
	
	public long auctionId;
	
	@Override
	public String toString() {
		return String.format("%s (%d)", super.toString(), auctionId);
	}
	
	@Override
	protected String getExplanation() throws EventTypeInvalidException {
		switch(this.getEventType()) {
			case AUCTION_ENDED:
				return String.format("auction %d ended", auctionId);
			case AUCTION_STARTED:
				return String.format("auction %d started", auctionId);
			default:
				throw new EventTypeInvalidException();	
		}		
	}
}
