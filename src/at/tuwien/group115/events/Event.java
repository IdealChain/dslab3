package at.tuwien.group115.events;

import java.io.Serializable;
import java.util.Date;

public abstract class Event implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public String id;
	public String type;
	public long timestamp;
	
	/**
	 * Returns the string representing this event for general output.
	 */
	@Override
	public String toString() {
		return String.format("%s/%s", id, type);
	}
	
	/**
	 * @return The string output representing this event inside the management client.
	 */
	public String toManagementString() {
		Date ts = new Date(timestamp);
		String ms = String.format("%s: %s", type, ts.toString());
		
		try {
			return String.format("%s - %s", ms, getExplanation());
		} catch (EventTypeInvalidException e) {
			return ms;
		}
	}
	
	/**
	 * @return Gets a textual explanation for the management client output.
	 */
	protected abstract String getExplanation() throws EventTypeInvalidException;

	/**
	 * @return The EventType enum of this events type.
	 * @throws EventTypeInvalidException
	 */
	public EventType getEventType() throws EventTypeInvalidException {
		try {
			return EventType.valueOf(type);
		} catch(IllegalArgumentException e) {
			throw new EventTypeInvalidException("Unknown event type", e);
		}
	}
}
