package at.tuwien.group115.events;

public class UserEvent extends Event
{
	private static final long serialVersionUID = 1L;
	
	public String userName;
	
	@Override
	public String toString() {
		return String.format("%s (%s)", super.toString(), userName);
	}
	
	@Override
	protected String getExplanation() throws EventTypeInvalidException {
		switch(this.getEventType()) {
			case USER_LOGIN:
				return String.format("user %s logged in", userName);
			case USER_LOGOUT:
				return String.format("user %s logged out", userName);
			case USER_DISCONNECTED:
				return String.format("user %s disconnected", userName);
			default:
				throw new EventTypeInvalidException();	
		}		
	}
}
