package at.tuwien.group115.events;

public class EventTypeInvalidException extends Exception {

	private static final long serialVersionUID = 1L;

	public EventTypeInvalidException() {}

	public EventTypeInvalidException(String msg) {
		super(msg);
	}

	public EventTypeInvalidException(String msg, Throwable e) {
		super(msg, e);
	}

}