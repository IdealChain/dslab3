package at.tuwien.group115.server;

import java.util.Calendar;
import org.apache.log4j.Logger;
import at.tuwien.group115.server.data.User;
import at.tuwien.group115.shared.Base64Helper;
import at.tuwien.group115.shared.BidCommand;
import at.tuwien.group115.shared.InsufficientParametersException;
import at.tuwien.group115.shared.NotLoggedInException;
import at.tuwien.group115.shared.Signature;
import at.tuwien.group115.shared.SignatureFormatInvalidException;
import at.tuwien.group115.shared.security.Generators;
import at.tuwien.group115.shared.security.ISecureChannel;
import at.tuwien.group115.shared.security.KeyReader;
import at.tuwien.group115.shared.security.KeyReaderException;
import at.tuwien.group115.shared.security.SignatureHelper;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;

public class ClientShell {
	
	private final static Logger log = Logger.getLogger(ClientShell.class);
	
	protected ServerData data;
	protected ClientTerminal terminal;
	
	public ClientShell(ServerData data, ClientTerminal terminal) {
		this.data = data;
		this.terminal = terminal;
	}
	
	public String executeCommand(String cmd) throws LoginException {
		
		try {
			if (cmd.startsWith("!list")) {
				return list(cmd.equals("!list all"));
			} else if (cmd.startsWith("!bid")) {
				return bid(extractParams(cmd, 2));
			} else if (cmd.startsWith("!login")) {
				if (terminal.clientUser != null)
					return String.format("You are already logged in as %s!", terminal.clientUser.getName());
				return "Unauthenticated login not possible anymore.";
			} else if (cmd.startsWith("!logout")) {
				return logout();
			} else if (cmd.startsWith("!create")) {
				return create(extractParams(cmd, 2));
			} else if (cmd.startsWith("!end") || cmd.startsWith("!exit")) {
				return end();
			} else if (cmd.startsWith("!getClientList")) {
				return getClientList();
			} else if (cmd.startsWith("!signedBid")) {
				return signedBid(extractParams(cmd, 4));
			} else if (cmd.isEmpty()) {
				return "Please give a command.";
			} else if (cmd.startsWith("!groupBid")) {
				return groupBid(extractParams(cmd, 2));
			} else if (cmd.startsWith("!confirm")) {
				return confirm(extractParams(cmd, 3));
			} else {
				String result = tryLogin(cmd);
				if (result == null)
					return "Unknown command.";
				return result;
			}
			
		} catch (NotLoggedInException e) {
			return "Error: you must login first!";
		} catch (InsufficientParametersException e) {
			return String.format("Error: expected %d arg(s)!", e.ExpectedParameters);
		}
	}

	/**
	 * Try to initiate secure channel (with handshake).
	 * @param cmd The received command, which is suspected to be a RSA handshake init message.
	 * @return Status message for the user, or null if the command could not be made sense of.
	 * @throws LoginException In case the handshake worked, but the user is not allowed to login.
	 */
	private String tryLogin(String cmd) throws LoginException {
		
		ISecureChannel channel = this.terminal.getChannel();
		if (channel != null && !channel.isSecure()) {
			try {
				AuthenticateClient auth = new AuthenticateClient(this.data);
				if (channel.initServer(cmd, this.data.getServerKey(), auth))
					return login(auth.getAuthenticatedUser(), auth.getClientPort());
			} catch (AssertionError e) {
				log.debug(String.format("No valid RSA handshake (%s)", e.getMessage()));
			} catch (LoginException e) {
				throw e;
			} catch (Exception e) {
				log.error("SecureChannel.initServer", e);
			}
		}
		
		return null;
	}
	
	private String login(User user, int port) throws LoginException {
		data.getLoggedinUsers().Login(user, terminal, data.getEventPublisher());
		terminal.clientUser = user;
		terminal.clientPort = port;
		return String.format("Successfully logged in as %s!", user.getName());
	}

	private String list(boolean all) {
		String msg = data.getAuctionManager().list(all);
		if (terminal.clientUser == null)
			return msg;

		Key secretKey = null;

		String keyPath = String.format("%s/%s.key", data.getClientKeyDirectory(), terminal.clientUser.getName());
		try {
			secretKey = KeyReader.readSecretKey(keyPath);
		} catch (FileNotFoundException e1) {
			log.error("Could not read key file");
		} catch (KeyReaderException e1) {
			log.error("There was a problem reading the key file");
		}
		
		if (secretKey == null)
			return msg;
		
		byte[] hash = null;
		try {
			hash = Generators.generateHMAC(msg.getBytes("UTF-8"), secretKey);
		} catch (InvalidKeyException e) {
			log.error("Invalid key");
		} catch (NoSuchAlgorithmException e) {
			log.error("No such algorithm");
		} catch (UnsupportedEncodingException e) {
			log.error("Unsupported encoding");
		}
		
		if (hash == null)
			return msg;

		return msg + " " + Base64Helper.encodeByte(hash);
	}

	private String end() {
		// ensure logout
		if (terminal.clientUser != null) {
			data.getLoggedinUsers().Logout(terminal.clientUser, data.getEventPublisher());
			terminal.resetTerminal();
		}
		
		terminal.closeTerminal();
		return "Bye!";
	}

	private String logout() throws NotLoggedInException {
		ensureLogin();
		
		data.getLoggedinUsers().Logout(terminal.clientUser, data.getEventPublisher());
		terminal.resetTerminal();
		return "Logged out.";
	}

	private String create(String[] args) throws NotLoggedInException {
		ensureLogin();
		
		try {
			int duration = Integer.parseInt(args[1]);
			
			String desc = args[2];
			
			Calendar deadline = Calendar.getInstance();
			deadline.add(Calendar.SECOND, duration);
			
			return data.getAuctionManager().create(deadline, desc, terminal.clientUser, data.getEventPublisher());
			
		} catch (NumberFormatException e) {
			return "No valid number!";
		}
	}

	private String groupBid(String[] args) throws NotLoggedInException {
		ensureLogin();
		
		try {
			int auctionID = Integer.parseInt(args[1]);
			double amount = Double.parseDouble(args[2]);
			
			return data.getAuctionManager().groupBid(auctionID, amount, 
					terminal.clientUser, data.getEventPublisher(), 
					data.getLoggedinUsers().getLoggedInUserCount());
			
		} catch (NumberFormatException e) {
			return "No valid number!";
		}
	}
	
	private String confirm(String[] args) throws NotLoggedInException {
		ensureLogin();
		
		try {
			int auctionID = Integer.parseInt(args[1]);
			double amount = Double.parseDouble(args[2]);
			String name = args[3];
			
			return data.getAuctionManager().confirmBid(auctionID, amount, name, terminal.clientUser);
			
		} catch (NumberFormatException e) {
			return "No valid number!";
		}
	}
	
	private String bid(String[] args) throws NotLoggedInException {
		ensureLogin();
		
		try {
			int auctionID = Integer.parseInt(args[1]);
			double amount = Double.parseDouble(args[2]);
			
			return data.getAuctionManager().bid(auctionID, amount, terminal.clientUser, data.getEventPublisher());
			
		} catch (NumberFormatException e) {
			return "No valid number!";
		}
	}
	
	private String signedBid(String[] args) throws NotLoggedInException {
		ensureLogin();
		
		try {
			int auctionID = Integer.parseInt(args[1]);
			double amount = Double.parseDouble(args[2]);
			
			Signature sigs[] = { new Signature(args[3]), new Signature(args[4]) };
			BidCommand bid = new BidCommand(auctionID, amount);
			
			long timestampMean = 0;
			
			// check all signatures
			for (Signature s : sigs) {
				User signer = data.getUserManager().getUser(s.getUsername());
				if (signer == null)
					return "User not known: " + s.getUsername();
				
				if (signer == terminal.clientUser)
					return "User signed his own bid!";
				
				timestampMean += s.getTimestamp();
				
				String timestamp = bid.getTimestamp(s.getTimestamp());
				if (!SignatureHelper.verifySignature(timestamp, s.getSignature(), signer.getPublicKey()))
					return "Signature not valid: " + s.toString(); 
			}
			
			timestampMean /= sigs.length;
			Calendar ts = Calendar.getInstance();
			ts.setTimeInMillis(timestampMean);
			log.info(String.format("Signed bid has been verified (%s / %s)", bid.toString(), ts.getTime().toString()));
			
			// signatures valid, add bid
			return data.getAuctionManager().bid(auctionID, amount, terminal.clientUser, data.getEventPublisher(), timestampMean);
			
		} catch (SignatureFormatInvalidException e) {
			log.warn(e.toString());
			return e.toString();
		} catch (NumberFormatException e) {
			log.warn(e.toString());
			return "No valid number!";
		} catch (InvalidKeyException e) {
			log.error(e.toString());
			return e.toString();
		} catch (NoSuchAlgorithmException e) {
			log.error(e.toString());
			return e.toString();
		} catch (SignatureException e) {
			log.error(e.toString());
			return e.toString();
		}
	}
	
	private String getClientList() throws NotLoggedInException {
		ensureLogin();
		
		return data.getLoggedinUsers().getClientList();
	}
	
	private void ensureLogin() throws NotLoggedInException {
		if (terminal == null || terminal.clientUser == null)
			throw new NotLoggedInException("User needs to be logged in for this operation");
	}
	
	private static String[] extractParams(String msg, int paramCount) throws InsufficientParametersException {
		
		String[] params = msg.split(" ", paramCount + 1);
		if (params.length < paramCount + 1)
			throw new InsufficientParametersException(paramCount);
		
		return params;
	}
}
