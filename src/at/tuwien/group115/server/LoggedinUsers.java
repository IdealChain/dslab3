package at.tuwien.group115.server;

import java.util.Hashtable;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import at.tuwien.group115.events.EventType;
import at.tuwien.group115.server.data.EventGenerator;
import at.tuwien.group115.server.data.EventPublisher;
import at.tuwien.group115.server.data.User;

public class LoggedinUsers {
	
	private final static Logger log = Logger.getLogger(LoggedinUsers.class);
	
	private Hashtable<User, ClientTerminal> loggedin = new Hashtable<User, ClientTerminal>();
	
	public void Login(User user, ClientTerminal terminal, EventPublisher pub) throws LoginException {
		
		synchronized(this) {
			if (loggedin.containsKey(user)) {
				ClientTerminal other = loggedin.get(user);			
				throw new LoginException(String.format("Client %s is already logged in from %s.", 
						user.getName(), other.getTerminalID()));
			}
			
			loggedin.put(user, terminal);
		}
		
		pub.publishEvent(EventGenerator.createUserEvent(user, EventType.USER_LOGIN));
		log.info(String.format("User %s logged in on %s.", 
				user.getName(), terminal.getTerminalID()));
	}
	
	public void Logout(User user, EventPublisher pub) {
		
		synchronized(this) {
			if (loggedin.remove(user) == null)
				return;
		}
		
		pub.publishEvent(EventGenerator.createUserEvent(user, EventType.USER_LOGOUT));
	}
	
	public void Disconnect(User user, EventPublisher pub) {
		
		synchronized(this) {
			if (loggedin.remove(user) == null)
				return;
		}
		
		pub.publishEvent(EventGenerator.createUserEvent(user, EventType.USER_DISCONNECTED));
	}
	
	public void DisconnectAll(EventPublisher pub) {
		
		synchronized(this) {
			for(Entry<User, ClientTerminal> u : loggedin.entrySet())
				pub.publishEvent(EventGenerator.createUserEvent(u.getKey(), EventType.USER_DISCONNECTED));
			
			loggedin.clear();
		}
		
	}
	
	public synchronized boolean isLoggedIn(User user) {
		return loggedin.containsKey(user);
	}
	
	public synchronized int getLoggedInUserCount() {
		return loggedin.size();
	}
	
	public synchronized String getClientList() {
		if (loggedin.isEmpty())
			return "No active clients!";
		
		String list = "Active Clients:";
		
		for(Entry<User, ClientTerminal> u : loggedin.entrySet()) {
			String socket = String.format("%s:%d", u.getValue().getIPAddress(), u.getValue().clientPort);
			list += String.format("\n%s - %s", socket, u.getKey().getName());
		}
		
		return list;
	}
}
