package at.tuwien.group115.server;

import java.io.IOException;
import java.net.Socket;
import org.apache.log4j.Logger;

import at.tuwien.group115.server.data.User;
import at.tuwien.group115.shared.security.ISecureChannel;
import at.tuwien.group115.shared.security.SecureChannel;
import at.tuwien.group115.shared.security.TCPChannel;

public class ClientTerminal implements Runnable {
	
	private final static Logger log = Logger.getLogger(ClientTerminal.class);
	
	protected Socket clientSocket;
	protected Server server;
	
	protected boolean isActive = true;
	protected User clientUser = null;
	protected int clientPort = 0;
	protected ISecureChannel clientChannel = null;

    public ClientTerminal(Socket clientSocket, Server server) {
        this.clientSocket = clientSocket;
        this.server = server;
    }
    
    public String getIPAddress() {
    	return this.clientSocket.getInetAddress().getHostAddress();
    }
    
    public String getTerminalID() {
    	String terminalID = this.getIPAddress();
    	
    	if (this.clientUser != null)
    		terminalID += String.format("|%d|%s", this.clientPort, this.clientUser.getName());
    	
    	return terminalID;
    }
    
    public synchronized boolean getIsActive() {
    	return this.isActive;
    }
    
    public ISecureChannel getChannel() {
    	return this.clientChannel;
    }
    
    /**
     * Marks the terminal for closing.
     */
    public synchronized void closeTerminal() {
    	this.isActive = false;
    }
    
    /**
     * Resets the terminal into non-loggedin, non-secure state.
     */
    public void resetTerminal() {
    	this.clientUser = null;
    	this.clientPort = 0;
    	if (this.clientChannel != null)
    		this.clientChannel.reset();
    }

    public void run() {
		
        try {
            log.info(String.format("Starting client thread (%s)...", this.getTerminalID()));
            clientChannel = new SecureChannel(new TCPChannel(clientSocket), true);
            
    		String cmd;
    		ClientShell shell = new ClientShell(this.server.getServerData(), this);
    		do {       		
    			if ((cmd = clientChannel.receive()) == null)
    				break;
    			
    			log.info(String.format("Received command (%s): %.20s", this.getTerminalID(), cmd));
    			
    			String ret = null;
    			boolean loginError = false;
    			try {
	    			// execute command
	    			ret = shell.executeCommand(cmd);
	    			
    			} catch (LoginException e) {
    				ret = "Error: " + e.getMessage(); 
    				loginError = true;
    			}
    			
    			clientChannel.send(ret);
    			
    			// in case of login error: reset channel after sending the error message
    			if (loginError)
    				this.resetTerminal();
    			
    		} while (this.getIsActive());

        } catch (IOException e) {
        	
        	if (!server.isStopped())
        		log.warn("Error during request processing", e);
        	
        } finally {
        	try {
        		log.info(String.format("Ending client thread (%s).", this.getTerminalID()));
        		
        		// ensure disconnect
        		if (this.clientUser != null) {
        			this.server.getServerData().getLoggedinUsers().Disconnect(
        					this.clientUser, 
        					this.server.getServerData().getEventPublisher());
        		}
        		
	        	if (this.clientChannel != null)
	        		this.clientChannel.close();
				
				if (this.server != null)
					this.server.clientSocketClosed(this.clientSocket);
				
        	} catch(IOException e) {
        		log.warn("IOException during close", e);
			}
        }
    }
}
