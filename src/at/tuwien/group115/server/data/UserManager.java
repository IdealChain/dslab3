package at.tuwien.group115.server.data;

import java.security.PublicKey;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.security.KeyReader;

public class UserManager {
	
	private final static Logger log = Logger.getLogger(UserManager.class);
	
	private Set<User> users = new HashSet<User>();
	
	public UserManager(String clientKeyDir) {

		// add users with public keys (=registered users)
		for (Entry<String, PublicKey> i : KeyReader.readPublicKeys(clientKeyDir).entrySet()) {
			
			User user = new User(i.getKey(), i.getValue(), this);
			if (users.add(user)) 
				log.info(String.format("Added user: %s [%s]", i.getKey(), i.getValue().getFormat()));
		}
	}
	
	public synchronized User getUser(String username) {
				
		// find existing user
		Iterator<User> it = users.iterator();
		while (it.hasNext()) {
			User i = it.next();
			if (i.getName().equals(username))
				return i;
		}
		
		return null;
	}
}