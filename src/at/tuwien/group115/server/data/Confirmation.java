package at.tuwien.group115.server.data;

import java.util.ArrayList;

public class Confirmation {
	
	protected ArrayList<User> confirmations = null;
	protected Auction auction = null;
	protected User groupBidAuthor = null;
	protected EventPublisher pub = null;
	protected double amount = 0.f;
	protected String result = null;
	
	public Confirmation(Auction auction, double amount, User user, EventPublisher pub) {
		this.auction = auction;
		this.amount = amount;
		this.groupBidAuthor = user;
		this.pub = pub;
		
		confirmations = new ArrayList<User>();
	}
	
	public boolean addConfirmation(User user) {
		if (confirmations.contains(user))
			return false;
		
		return confirmations.add(user);
	}
	
	public ArrayList<User> getConfirmedUsers() {
		return confirmations;
	}
	
	public boolean isConfirmationCompleted() {
		if (confirmations.size() >= 2)
			return true;
		
		return false;
	}
	
	public Auction getAuction() {
		return auction;
	}
	
	public User getGroupBidAuthor() {
		return groupBidAuthor;
	}
	
	public EventPublisher getPublisher() {
		return pub;
	}
	
	public double getAmount() {
		return amount;
	}
	
	public boolean hasResult() {
		return result != null;
	}
	
	public String getResult() {
		return result;
	}
	
	public String setResult(String value) {
		result = value;
		return result;
	}
}
