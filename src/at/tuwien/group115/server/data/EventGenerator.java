package at.tuwien.group115.server.data;

import java.util.Date;

import at.tuwien.group115.events.AuctionEvent;
import at.tuwien.group115.events.BidEvent;
import at.tuwien.group115.events.Event;
import at.tuwien.group115.events.EventType;
import at.tuwien.group115.events.UserEvent;

public class EventGenerator {
	
	private static int nextID = 1;
	
	private synchronized static Event initEvent(Event e, EventType t) {
		e.id = String.format("auction-event:%d", nextID++);
		e.type = t.toString();
		e.timestamp = new Date().getTime();
		return e;
	}

	public static Event createAuctionEvent(Auction a, EventType t) {
		AuctionEvent e = new AuctionEvent();
		e.auctionId = a.getId();
		return initEvent(e, t);
	}
	
	public static Event createUserEvent(User u, EventType t) {
		UserEvent e = new UserEvent();
		e.userName = u.getName();
		return initEvent(e, t);
	}
	
	public static Event createBidEvent(Bid b, Auction a, EventType t) {
		BidEvent e = new BidEvent();
		e.userName = b.getBidder().getName();
		e.auctionId = a.getId();
		e.price = b.getAmount();
		return initEvent(e, t);
	}
}
