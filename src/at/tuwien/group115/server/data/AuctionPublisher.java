package at.tuwien.group115.server.data;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;

import org.apache.log4j.Logger;

import at.tuwien.group115.billing.IBillingServer;
import at.tuwien.group115.billing.IBillingServerSecure;
import at.tuwien.group115.shared.PropertiesInvalidException;
import at.tuwien.group115.shared.RegistryHelper;

public class AuctionPublisher {

	private final static Logger log = Logger.getLogger(AuctionPublisher.class);
	
	String billingBindingName = null;
	IBillingServer server = null;

	public AuctionPublisher(String billingBindingName) {
		this.billingBindingName = billingBindingName;
	}
	
	public void billAuction(String user, long auctionID, double price) {
		
		log.info(String.format("Calling billing server for user " +
				"'%s' (auction %d) with price: %f", user, auctionID, price));
		
		if (this.server == null)
			getRemoteServer();
		
		if (this.server == null) {
			log.warn("Can't publish - billing server not running");
			return;
		}
		
		try {
			IBillingServerSecure sec = this.server.login("auctionserver", "auctionserver");
			sec.billAuction(user, auctionID, price);
		} catch (RemoteException e) {
			log.warn("Remote error during calling billing" +
					" server for auction information", e);
			this.server = null;
		}
	}

	private void getRemoteServer() {
	
		// obtain RMI registry
		Registry registry = null;
		try {
			registry = RegistryHelper.getRegistry();
		} catch (PropertiesInvalidException e) {
			log.warn("registry.properties file invalid: " + e.getMessage(), e.getCause());
			return;
		} catch (RemoteException e) {
			log.warn("Registry could not be obtained", e);
			return;
		}
		
		// obtain remote object
		Remote remoteObject = null;
		try {
			remoteObject = registry.lookup(this.billingBindingName);
			if (remoteObject instanceof IBillingServer) {
				server = (IBillingServer)remoteObject;
				log.info("Remote analytics server obtained successfully");
			} else {
				log.warn("Remote object not an analytics server");
			}
			
		} catch (NotBoundException e) {
			log.warn("No billing server class bound in registry");
		} catch (RemoteException e) {
			log.warn("Error during lookup of analytics server", e);
		}
	}
}
