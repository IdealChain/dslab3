package at.tuwien.group115.server.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import at.tuwien.group115.events.EventType;

public class AuctionManager {
	
	// all auctions
	private TreeMap<Integer, Auction> auctions = new TreeMap<Integer, Auction>();
	private Map<Integer, Confirmation> groupBids = new HashMap<Integer, Confirmation>();
	
	// already fully processed auctions (= ended and announced)
	private Set<Integer> processedAuctions = new HashSet<Integer>();
	
	private int nextID = 1;
	private final static Logger log = Logger.getLogger(AuctionManager.class);
	
	public synchronized String list() {
		return list(false);
	}
	
	public synchronized String list(boolean all) {
		
		if (auctions.size() < 1)
			return "No auctions here.";
		
		if (processedAuctions.size() >= auctions.size() && !all)
			return "No active auctions here.";
		
		String list = "";
		Iterator<Entry<Integer, Auction>> it = auctions.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, Auction> entry = it.next();
			
			if (entry.getValue().hasEnded() && !all)
				continue;
			
			list += entry.getValue().toString();
			if (it.hasNext())
				list += '\n';
		}

		return list;
	}
	
	public String create(Calendar deadline, String description, User owner, EventPublisher pub) {
		
		Auction auction;
		
		synchronized(this) {
			int id = nextID++;
			auction = new Auction(id, description, owner, deadline);
			auctions.put(id, auction);
		}
		
		pub.publishEvent(EventGenerator.createAuctionEvent(auction, EventType.AUCTION_STARTED));
		
		return String.format("An auction '%s' with id %d has been created and will end on %s.",
				auction.getDescription(),
				auction.getId(),
				auction.getDeadline().getTime().toString());
	}
	
	public String bid(int auctionID, double amount, User bidder, EventPublisher pub) {
		return this.bid(auctionID, amount, bidder, pub, System.currentTimeMillis());
	}
	
	public String bid(int auctionID, double amount, User bidder, EventPublisher pub, long timestamp) {
		
		Auction auction = null;
		Bid previousBid = null;
		Bid myBid = null;
		
		synchronized(this) {
			if (!auctions.containsKey(auctionID))
				return String.format("There is no auction with ID %d!", auctionID);
			
			auction = auctions.get(auctionID);
			
			if (auction.hasEnded() && timestamp > auction.getDeadlineMillis())
				return String.format("Bidding is already closed for auction ID %d!", auctionID);
			
			previousBid = auction.getCurrentBid();
			if (auction.tryBid(amount, bidder)) {
				myBid = auction.getCurrentBid();
				
				// remove ended auction to be processed again
				if (auction.hasEnded())
					processedAuctions.remove(auction.getId());
			}
		}
		
		if (myBid != null) {
			
			if (previousBid.getBidder() != null) {
				
				// inform old bidder of his fate
				String msg = String.format("!new-bid %s" , auction.getDescription());
				previousBid.getBidder().queueNotification(msg);
				pub.publishEvent(EventGenerator.createBidEvent(previousBid, auction, EventType.BID_OVERBID));
			}
			
			// successfully bidden
			pub.publishEvent(EventGenerator.createBidEvent(myBid, auction, EventType.BID_PLACED));
			return String.format("You successfully bid with %.2f on '%s'.",
					amount, auction.getDescription());
		} 
		
		// not overbidden
		return String.format("You unsuccessfully bid with %.2f on '%s'. Current highest bid is %.2f.",
				amount, auction.getDescription(), previousBid.getAmount());
		
	}
	
	public synchronized String groupBid(int auctionID, double amount, User bidder, EventPublisher pub, int loggedInUsers) {
		if (groupBids.size()+1 > loggedInUsers)
			return String.format("!rejected There are too much group bids (%d) " +
					"for logged in users (%d)!", groupBids.size(), loggedInUsers);
		if (!auctions.containsKey(auctionID))
			return String.format("!rejected There is no auction with ID %d!", auctionID);
		
		if (groupBids.containsKey(auctionID))
			return String.format("!rejected To the auction with ID %d a group bid already exists!", auctionID);
		
		Confirmation conf = new Confirmation(auctions.get(auctionID), amount, bidder, pub);
		
		groupBids.put(auctionID, conf);
		
		// start deadlock prevention
		Timer timer = new Timer();
		timer.schedule(new PreventDeadlock(auctionID, timer, this), 0, 18000);
		
		return "Group bid has been initiated, waiting to be confirmed.";
	}	
	
	public synchronized String confirmBid(int auctionID, double amount, String user, User client)
	{
		if (!auctions.containsKey(auctionID))
			return String.format("!rejected There is no auction with ID %d!", auctionID);
		if (!groupBids.containsKey(auctionID))
			return String.format("!rejected There is no group bid with ID %d!", auctionID);
		
		Confirmation conf = groupBids.get(auctionID);

		if (conf.getAmount() != amount)
			return String.format("!rejected The bid amount is " +
					"%.2f but you confirmed with %.2f!", conf.getAmount(), amount);
		if (conf.isConfirmationCompleted())
			return String.format("!rejected The group bid for auction %d has already been confirmed!", auctionID);
		if (!conf.getGroupBidAuthor().getName().equals(user.trim()))
			return String.format("!rejected The creator of the group bid is %s who " +
					"is not %s", conf.getGroupBidAuthor().getName(), user);
		if (conf.getGroupBidAuthor() == client)
			return "!rejected cannot confirm as a groupbid author";
		if (!conf.addConfirmation(client))
			return String.format("!rejected user %s already placed a bet!", user);
		
		// wait until confirmation is completed or removed by deadlock prevention
		while (groupBids.containsKey(auctionID) && !conf.isConfirmationCompleted()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				log.warn(e);
			}
		}
		
		// notify other waiting threads
		this.notifyAll();
		
		// if another thread already yielded a result, return that
		if (conf.hasResult())
			return conf.getResult();
		
		// first thread: decide on result
		if (!groupBids.containsKey(auctionID))
			return conf.setResult("!rejected Deadlock prevention");
		
		groupBids.remove(auctionID);
		Auction auction = auctions.get(auctionID);
		if (auction.hasEnded() || auction.getCurrentBid().getAmount() >= amount)
			return conf.setResult("!rejected The auction has already ended or the current bid is higher");
		
		bid(auctionID, amount, conf.getGroupBidAuthor(), conf.getPublisher());
		return conf.setResult("!confirmed");
	}
	
	public ArrayList<Auction> getEndedAuctions(EventPublisher pub, AuctionPublisher auct) {			
		ArrayList<Auction> ended = new ArrayList<Auction>();
		
		synchronized(this) {
			// fetch ended auctions that were not already processed
			for(Map.Entry<Integer, Auction> entry : auctions.entrySet()) {
				if (entry.getValue().hasEnded() && !processedAuctions.contains(entry.getKey()))
					ended.add(entry.getValue());
			}
			
			// add ended auctions to processed set
			for(Auction a : ended)
				processedAuctions.add(a.getId());
		}
		
		// send auction ended / winner events
		for(Auction a : ended) {
			pub.publishEvent(EventGenerator.createAuctionEvent(a, EventType.AUCTION_ENDED));
			if (a.getCurrentBid().getBidder() != null)
			{
				pub.publishEvent(EventGenerator.createBidEvent(a.getCurrentBid(), a, EventType.BID_WON));
				auct.billAuction(a.getCurrentBid().getBidder().getName(), a.getId(), a.getCurrentBid().getAmount());
			}
		}

		return ended;		
	}
	
	private class PreventDeadlock extends TimerTask
	{
		private int counter = 0;
		private int aID = 0;
		private Timer timer = null;
		private AuctionManager parent = null;
		
		public PreventDeadlock(int aID, Timer timer, AuctionManager parent) {
			this.aID = aID;
			this.timer = timer;
			this.parent = parent;
		}
		
		@Override
		public void run() {
			synchronized(parent) {
				counter++;
				if (counter <= 1)
					return;
				
				if (groupBids.containsKey(aID)) {
					groupBids.remove(aID);
					log.info(String.format("removed group bid with auctionID %d", aID));
					timer.cancel();
					parent.notifyAll();
				}
				counter++;
			}
		}
		
	}
}
