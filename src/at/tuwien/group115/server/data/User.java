package at.tuwien.group115.server.data;

import java.security.PublicKey;
import java.util.LinkedList;

public class User {
	
	protected String name;
	protected PublicKey key;
	protected UserManager manager;
	protected LinkedList<String> pendingNotifications = new LinkedList<String>();
	
	public User(String name, PublicKey key, UserManager manager) {
		this.name = name;
		this.key = key;
		this.manager = manager;
	}
	
	public String getName() {
		return this.name;
	}
	
	public PublicKey getPublicKey() {
		return this.key;
	}
	
	public synchronized void queueNotification(String msg) {
		this.pendingNotifications.add(msg);
	}
	
	public synchronized LinkedList<String> popPendingNotifications() {
		LinkedList<String> n = this.pendingNotifications;
		this.pendingNotifications = new LinkedList<String>();
		return n;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof User))
			return false;
		
		return ((User)object).getName() == this.getName();
	}
	
	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}

}
