package at.tuwien.group115.server.data;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;

import org.apache.log4j.Logger;

import at.tuwien.group115.analytics.IAnalyticsServer;
import at.tuwien.group115.events.Event;
import at.tuwien.group115.shared.RegistryHelper;
import at.tuwien.group115.shared.PropertiesInvalidException;

public class EventPublisher {
	
	private final static Logger log = Logger.getLogger(EventPublisher.class);
	
	String analyticsBindingName;
	IAnalyticsServer server;

	public EventPublisher(String analyticsBindingName) {
		this.analyticsBindingName = analyticsBindingName;
	}
	
	public void publishEvent(Event event) {
		
		log.info("Publishing event " + event.toString());
		
		if (this.server == null)
			getRemoteServer();
		
		if (this.server == null) {
			log.warn("Can't publish - analytics server not running");
			return;
		}
		
		try {
			this.server.processEvent(event);
		} catch (RemoteException e) {
			log.warn("Remote error during event pushing", e);
			this.server = null;
		}
	}

	private void getRemoteServer() {
	
		// obtain RMI registry
		Registry registry = null;
		try {
			registry = RegistryHelper.getRegistry();
		} catch (PropertiesInvalidException e) {
			log.warn("registry.properties file invalid: " + e.getMessage(), e.getCause());
			return;
		} catch (RemoteException e) {
			log.warn("Registry could not be obtained", e);
			return;
		}
		
		// obtain remote object
		Remote remoteObject;
		try {
			remoteObject = registry.lookup(this.analyticsBindingName);
			if (remoteObject instanceof IAnalyticsServer) {
				server = (IAnalyticsServer)remoteObject;
				log.info("Remote analytics server obtained successfully");
			} else {
				log.warn("Remote object not an analytics server");
			}
			
		} catch (NotBoundException e) {
			log.warn("No analytics server class bound in registry");
		} catch (RemoteException e) {
			log.warn("Error during lookup of analytics server", e);
		}
	}
}
