package at.tuwien.group115.server.data;

public class Bid {

	protected double amount;
	protected User bidder;
	
	public Bid() {
		amount = 0;
		bidder = null;
	}
	
	public Bid(double amount, User bidder) {
		this.amount = amount;
		this.bidder = bidder;
	}

	public double getAmount() {
		return amount;
	}

	public User getBidder() {
		return bidder;
	}
	
	public String toString() {
		String bidder = "none";
		if (this.bidder != null)
			bidder = this.bidder.getName();
		
		return String.format("%.2f %s", amount, bidder);
	}
	
}
