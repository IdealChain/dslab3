package at.tuwien.group115.server.data;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

public class Auction {

	protected int id;
	protected String description;
	protected User owner;
	protected Calendar deadline;
	
	private Set<User> bidders = new HashSet<User>(); 
	private Bid currentBid = new Bid();
	
	public Auction(int id, String description, User owner, Calendar deadline) {
		this.id = id;
		this.description = description;
		this.owner = owner;
		this.deadline = deadline;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public User getOwner() {
		return owner;
	}

	public Calendar getDeadline() {
		return deadline;
	}
	
	public long getDeadlineMillis() {
		return deadline.getTimeInMillis();
	}
	
	public Bid getCurrentBid() {
		return this.currentBid;
	}
	
	public Set<User> getBidders() {
		return this.bidders;
	}
	
	public String toString() {
		
		String s = String.format("%d. '%s' %s %s %s", 
				this.id,
				this.description, 
				this.owner.getName(),
				this.deadline.getTime().toString(),
				this.currentBid.toString());
		
		if (hasEnded())
			s += " (ended)";
		
		return s;
	}
	
	public boolean tryBid(double amount, User bidder) {
		
		if (amount > this.currentBid.getAmount()) {
			this.currentBid = new Bid(amount, bidder);
			this.bidders.add(bidder);
			return true;
		}
		
		return false;
	}
	
	public boolean hasEnded() {
		return getDeadlineMillis() < System.currentTimeMillis();
	}
}
