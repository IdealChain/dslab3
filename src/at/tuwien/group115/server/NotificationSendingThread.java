package at.tuwien.group115.server;

import java.util.ArrayList;
import java.util.Locale;

import org.apache.log4j.Logger;

import at.tuwien.group115.server.data.Auction;

public class NotificationSendingThread implements Runnable {
	
	private final static Logger log = Logger.getLogger(NotificationSendingThread.class);

	protected ServerData data;
	protected boolean isStopped = false;
	
	public NotificationSendingThread(ServerData data) {
		this.data = data;
	}
	
	public void run() {
		
		log.info("Started.");
		
		while (!this.isStopped()) {
			
			// get all ended auctions and queue UDP notifications
			ArrayList<Auction> ended = data.getAuctionManager().getEndedAuctions(
					data.getEventPublisher(), data.getAuctionPublisher());
			
			for (Auction a : ended) {
				
				log.info(String.format("Auction %d '%s' ended.", 
						a.getId(), a.getDescription()));
				
				notifyUsers(a);
			}
			
			// sleep
			try {
				Thread.sleep(500);
			} catch (InterruptedException exp) {
				log.warn(exp);
			}
			
		}
		
		log.info("Stopped.");

	}
	
	private void notifyUsers(Auction a) {
		
		// send notification to winner (if there is one) and owner of the auction
		String msg = String.format(Locale.ENGLISH, "!auction-ended %s %.2f %s",
				a.getCurrentBid().getBidder() == null ? "Nobody" : a.getCurrentBid().getBidder().getName(),
				a.getCurrentBid().getAmount(),
				a.getDescription().toString());
		
		if (a.getCurrentBid().getBidder() != null && a.getCurrentBid().getBidder() != a.getOwner())
			a.getCurrentBid().getBidder().queueNotification(msg);
		
		a.getOwner().queueNotification(msg);
	}
	
	public synchronized boolean isStopped() {
		return this.isStopped;
	}

    public synchronized void stop() {
    	this.isStopped = true;
    }

}
