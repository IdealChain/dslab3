package at.tuwien.group115.server;

import java.security.PublicKey;

import org.apache.log4j.Logger;

import at.tuwien.group115.server.data.User;
import at.tuwien.group115.shared.security.IAuthenticateClient;

public class AuthenticateClient implements IAuthenticateClient {
	
	private final static Logger log = Logger.getLogger(AuthenticateClient.class);
	
	private ServerData data;
	
	private User user = null;
	private int clientPort = 0;
	private String error = null;
	
	public AuthenticateClient(ServerData data) {
		this.data = data;
	}
	
	public User getAuthenticatedUser() {
		return this.user;
	}
	
	public int getClientPort() {
		return this.clientPort;
	}
	
	public String getError() {
		return this.error;
	}

	@Override
	public PublicKey getPublicKey(String username) {
		User user = data.getUserManager().getUser(username);
		if (user == null)
			this.error = String.format("No user '%s' known to this server", username);
		
		if (this.error != null) {
			log.info(this.error);
			return null;
		}
		return user.getPublicKey();
	}

	@Override
	public void clientAuthenticated(String username, int clientPort) {
		this.user = data.getUserManager().getUser(username);
		assert this.user != null : "user does not exist";
		this.clientPort = clientPort;
	}

}
