package at.tuwien.group115.server;

import java.security.PrivateKey;

import at.tuwien.group115.server.data.AuctionManager;
import at.tuwien.group115.server.data.AuctionPublisher;
import at.tuwien.group115.server.data.EventPublisher;
import at.tuwien.group115.server.data.UserManager;

public class ServerData {

	protected LoggedinUsers loggedinUsers = new LoggedinUsers();
	protected UserManager userManager = null;
	protected AuctionManager auctionManager = new AuctionManager();
	protected EventPublisher eventPublisher = null;
	protected AuctionPublisher auctionPublisher = null;
	protected PrivateKey serverKey = null;
	protected String clientKeyDirectory = null;
	
	public ServerData(String analyticsBindingName, String billingBindingName, PrivateKey serverKey, String clientKeyDir) {
		this.userManager = new UserManager(clientKeyDir);
		this.eventPublisher = new EventPublisher(analyticsBindingName);
		this.auctionPublisher = new AuctionPublisher(billingBindingName);
		this.serverKey = serverKey;
		this.clientKeyDirectory = clientKeyDir;
	}
	
	public LoggedinUsers getLoggedinUsers() {
		return loggedinUsers;
	}
	public UserManager getUserManager() {
		return userManager;
	}
	public AuctionManager getAuctionManager() {
		return auctionManager;
	}
	
	public EventPublisher getEventPublisher() {
		return eventPublisher;
	}
	
	public AuctionPublisher getAuctionPublisher() {
		return auctionPublisher;
	}
	
	public PrivateKey getServerKey() {
		return serverKey;
	}
	
	public String getClientKeyDirectory() {
		return clientKeyDirectory;
	}
}
