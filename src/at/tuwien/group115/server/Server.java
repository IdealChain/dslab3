package at.tuwien.group115.server;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

public class Server implements Runnable {
	
	private final static Logger log = Logger.getLogger(Server.class);
	
	protected int serverPort;
	protected ServerSocket serverSocket;
	protected boolean isStopped = false;
	protected ExecutorService threadPool = Executors.newCachedThreadPool();
	ArrayList<Socket> clientSockets = new ArrayList<Socket>();
	protected ServerData serverData;
	
	public Server(int port, ServerData serverData) {		
		this.serverPort = port;
		this.serverData = serverData;
	}
	
	public ServerData getServerData() {
		return serverData;
	}

	@Override
	public void run() {
		openServerSocket();
		
		NotificationSendingThread auctionsCompleteThread = new NotificationSendingThread(serverData);
		new Thread(auctionsCompleteThread).start();
		
		while(!this.isStopped()) {
			
			Socket clientSocket = null;
			
			try {
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				if(this.isStopped()) 
					break;
				
				log.error("Error accepting client connection: " + e.toString());
				break;
			}

			synchronized(clientSockets) {
				clientSockets.add(clientSocket);				
			}
			
			this.threadPool.execute(new ClientTerminal(clientSocket, this));
		}
		
		this.threadPool.shutdown();
		
		// end auctions complete thread
		if (auctionsCompleteThread != null)
			auctionsCompleteThread.stop();
		
		// close all open client sockets
		synchronized(clientSockets) {
			log.info(String.format("Closing %d open client sockets.", clientSockets.size()));
			for (Socket s : clientSockets) {
				try {
					if (s != null)
						s.close();
				} catch (IOException e) {}
			}
		}
		
		log.info("Server stopped.") ;
	}
	
	public void clientSocketClosed(Socket socket) {
		
		// the client properly ended the session => can remove its socket from the cleanup list
		synchronized(clientSockets) {
			clientSockets.remove(socket);
		}
	}
	
	public synchronized boolean isStopped() {
		return this.isStopped;
	}

	public synchronized void stop() {
		this.isStopped = true;
		try {
			if (this.serverSocket != null) {
				this.serverSocket.close();
				this.serverSocket = null;
			}
		} catch (IOException e) {
			log.error("Error closing server socket", e);
		}
	}

	private synchronized void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
			log.info(String.format("Started server socket (%d)", this.serverPort));
		} catch (IOException e) {
			log.fatal("Cannot bind to port", e);
		}
	}

}
