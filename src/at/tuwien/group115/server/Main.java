package at.tuwien.group115.server;

import java.io.FileNotFoundException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.util.Arrays;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.Utility;
import at.tuwien.group115.shared.security.KeyReader;
import at.tuwien.group115.shared.security.KeyReaderException;

public class Main {
	
	private final static Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		int tcpPort = 0;
		
		if (args.length < 5 || args[1].isEmpty() || args[2].isEmpty() || args[3].isEmpty() || args[4].isEmpty())
			usage();
		
		try {
			tcpPort = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {}
		
		if (tcpPort < 1 || tcpPort > 65535)
			usage();
		
		// configure basic log4j console logger
		BasicConfigurator.configure();
		
		// create server specific data structures
		ServerData data = createData(args[1], args[2], args[3], args[4]);
		
		run(tcpPort, data);
	}
	
	public static void usage() {
		System.err.println(String.format(
				"Usage: java %s <tcp-port> <analytics-bindingName> <billing-bindingName> <server-key> <client-keydir>", 
				Main.class.getName()));
		System.exit(1);
	}
	
	public static ServerData createData(String analyticsBindingName, String billingBindingName, String serverKey, String clientKeyDir) {
		
		try {
			PrivateKey key = KeyReader.readPrivateKey(serverKey);
			return new ServerData(analyticsBindingName, billingBindingName, key, clientKeyDir);
			
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the server key file: " + e.getMessage());
			
		} catch (KeyReaderException e) {
			Throwable inner = e.getCause().getCause();
			if (inner != null && inner instanceof NoSuchProviderException) {
				System.out.println("Could not decrypt key: " + inner.getMessage());
			} else {
				System.out.println("Could not read the server key - did you enter the right passphrase?");
			}
		}
		
		System.exit(1);
		return null;
	}	

	private static void run(int tcpPort, ServerData data) {
		
		String cmd = null;
		boolean exit = false;
		
		do {
			System.out.println("Starting server, enter !exit to quit, !close to close the socket...");
			Server server = new Server(tcpPort, data);
			new Thread(server).start();
	
			cmd = Utility.waitForCommand(Arrays.asList("!end", "!exit", "!close"));
			exit = (cmd == null || !cmd.equals("!close"));
			
			log.info("Stopping server...");
			server.stop();
			
			data.getLoggedinUsers().DisconnectAll(data.getEventPublisher());
				
			if (!exit) {
				
				System.out.println("Socket closed, enter !exit to quit, !open to reopen socket...");
				cmd = Utility.waitForCommand(Arrays.asList("!end", "!exit", "!open"));
				exit = (cmd == null || !cmd.equals("!open"));
			}
			
		} while (!exit);
	}
}
