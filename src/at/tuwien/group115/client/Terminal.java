package at.tuwien.group115.client;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;

import at.tuwien.group115.client.offline.ISecureChannelCreator;
import at.tuwien.group115.client.offline.OfflineModeChannel;
import at.tuwien.group115.shared.security.ClientCredentials;
import at.tuwien.group115.shared.security.Generators;
import at.tuwien.group115.shared.security.ISecureChannel;
import at.tuwien.group115.shared.security.KeyReader;
import at.tuwien.group115.shared.security.KeyReaderException;
import at.tuwien.group115.shared.security.SecureChannel;
import at.tuwien.group115.shared.security.TCPChannel;

public class Terminal {
	
	private final static Logger log = Logger.getLogger(Terminal.class);
	
	private final static Pattern loginPattern = Pattern.compile("!login ([a-zA-Z0-9_\\-]+)");
	
	private String server;
	private int serverPort;
	private int clientPort;
	
	private PublicKey serverKey;
	private String clientKeyDir;
	
	private OfflineModeChannel channel = null;
	
	public Terminal(String server, int serverPort, int clientPort, String serverKey, String clientKeyDir)
	 throws KeyReaderException, FileNotFoundException {

		this.server = server;
		this.serverPort = serverPort;
		this.clientPort = clientPort;
		this.serverKey = KeyReader.readPublicKey(serverKey);
		this.clientKeyDir = clientKeyDir;
	}

	public void run() throws IOException {
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		
		channel = new OfflineModeChannel(new ISecureChannelCreator() {
			@Override
			public ISecureChannel createSecureChannel() throws IOException {
				Socket socket = new Socket(server, serverPort);
				return new SecureChannel(new TCPChannel(socket), false);
			}
		});
		
		try {
			int listFailCounter = 0;
			
			while (true) {
				
				String cmd;
				
				if (listFailCounter == 1) {
					// repeat list command one time
					cmd = "!list";
					
				} else {
					listFailCounter = 0;
					
					// ready for a new command
					if (this.channel.isSecure())
						System.out.print(channel.getUsername());
					if (this.channel.isOffline())
						System.out.print("(offline)");
					System.out.print("> ");
					
					if ((cmd = stdIn.readLine()) == null) 
						break;
				}
				
				String resp;
				Matcher login = loginPattern.matcher(cmd);
				
				// lock on offline mode channel to not get interrupted between send() and receive()
				synchronized(channel) {
				
					// catch and redirect login message to handshake
					if (login.matches() && !channel.isSecure()) {
						
						if (!this.login(login.group(1)))
							continue;
						
					} else {
					
						// send user command
						channel.send(cmd);
						
						// reset channel in case we are logging out (response is already unencrypted)
						if (this.channel.isSecure() && (cmd.startsWith("!logout") || cmd.startsWith("!end") || cmd.startsWith("!exit")))
							channel.reset();
					}
					
					// read response
					try {
						resp = channel.receive();
						if (resp == null)
							break;
					} catch (SocketException e) {
						break;
					}
				}
				
				// verify integrity of the list response
				if (cmd.startsWith("!list") && this.channel.isSecure() && !this.channel.isOffline())
				{
					String verified = this.verifyListResponse(resp);
					if (verified != null) {
						// print verified message (without hash)
						listFailCounter = 0;
						resp = verified;
					} else {
						// print original received message and repeat request
						listFailCounter++;
					}
				}
				
				// print response
				if (resp != null)
					System.out.println(resp);
				
				// in case of login, but an error message, reset the channel
				if (login.matches() && resp.startsWith("Error")) {
					channel.reset();
				}
			}
			
		} finally {
			try {
				if (stdIn != null)
					stdIn.close();
				if (channel != null) {
					channel.reset();
					channel.close();
				}
			} catch(IOException e) {
				log.warn("IOException during close", e);
			}
		}
	}
	
	private String verifyListResponse(String resp) {
		
		// pre hash character
		// https://www.infosys.tuwien.ac.at/teaching/courses/dslab/forum/viewtopic.php?pid=1253
		int hashPos = resp.lastIndexOf(" ");
		if (hashPos == -1) {
			log.error("List response did not end with a HMAC");
			return null;
		}
		
		// extract message and hash
		String msg = resp.substring(0, hashPos);
		String rcvHash = resp.substring(hashPos + 1, resp.length());
		
		// receivedHash is the HMAC that was sent by the communication partner
		byte[] receivedHash;
		try {
			 receivedHash = Base64.decode(rcvHash.getBytes("UTF-8"));
		} catch (Exception e) {
			log.error("Could not decode HMAC " + rcvHash, e);
			return null;
		}
		
		String keyPath = String.format("%s/%s.key", this.clientKeyDir, channel.getUsername());
		Key secretKey = null;
		try {
			secretKey = KeyReader.readSecretKey(keyPath);
		} catch (KeyReaderException e) {
			log.error("Could not read key from file " + keyPath);
			return null;
		} catch (FileNotFoundException e) {
			log.error("Key file not found " + keyPath);
			return null;
		}
		
		byte[] computedHash = null;
		try {
			computedHash = Generators.generateHMAC(msg.getBytes("UTF-8"), secretKey);
		} catch (InvalidKeyException e) {
			log.error("Invalid key");
			return null;
		} catch (NoSuchAlgorithmException e) {
			log.error("No such alogrithm");
			return null;
		} catch (UnsupportedEncodingException e) {
			log.error("Unsupported encoding");
			return null;
		}
		
		if (MessageDigest.isEqual(computedHash,  receivedHash))
			return msg;
		
		return null;
	}
	
	private boolean login(String username) throws IOException {
		
		String keyPath = String.format("%s/%s.pem", clientKeyDir, username);
		try {
			PrivateKey myKey = KeyReader.readPrivateKey(keyPath);
			ClientCredentials cred = new ClientCredentials(username, clientPort, myKey, serverKey);
			if (channel.login(cred))
				return true;
			
		} catch (FileNotFoundException e) {
			System.out.println(String.format("Client: User key file not found ('%s')", keyPath));
			
		} catch (KeyReaderException e) {
			System.out.println("Client: Could not read the user key - did you enter the right passphrase?");
								
		} catch (InvalidKeyException e) {
			System.out.println(String.format("Client: The user key seems not to be valid (%s)", e.getMessage()));
			
		} catch (Exception e) {
			log.fatal("Secure channel setup", e);
			System.exit(-1);
		}
		
		return false;
	}	
}
