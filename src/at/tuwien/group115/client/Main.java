package at.tuwien.group115.client;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.security.KeyReaderException;

public class Main {
	
	private final static Logger log = Logger.getLogger(Main.class);

	public static void main(String[] args) {
		String server = null;
		int serverPort = 0;
		int clientPort = 0;
		String serverKey = null;
		String clientKeyDir = null;
		
		try {
			if (args.length >= 5) {
				server = args[0];
				serverPort = Integer.parseInt(args[1]);
				clientPort = Integer.parseInt(args[2]);
				serverKey = args[3];
				clientKeyDir = args[4];
			}
		} catch (NumberFormatException e) {}
		
		if (server == null || server.length() < 1)
			usage();
		
		if (serverKey == null || serverKey.length() < 1)
			usage();
		
		if (clientKeyDir == null || clientKeyDir.length() < 1)
			usage();
		
		if (serverPort < 1 || serverPort > 65535)
			usage();
		
		if (clientPort < 1 || clientPort > 65535)
			usage();
		
		// configure basic log4j console logger
		BasicConfigurator.configure();

		run(server, serverPort, clientPort, serverKey, clientKeyDir);
	}
	
	public static void usage() {
		System.err.println(String.format("Usage: java %s <server> <server-port> <client-port> <server-keypub> <client-keydir>", Main.class.getName()));
		System.exit(1);
	}
	
	public static void run(String server, int serverPort, int clientPort, String serverKey, String clientKeyDir) {
		Socket s = null;
		
		try {
			new Terminal(server, serverPort, clientPort, serverKey, clientKeyDir).run();
			
		} catch (ConnectException e) {
			log.fatal(String.format("Could not connect to %s:%d (%s)", server, serverPort, e.getMessage()));
			
		} catch (KeyReaderException e) {
			log.fatal("Something is not quite right with your keysetup...", e.getCause());
			
		} catch (Exception e) {
			log.fatal("Error", e);
			
		} finally {
			try {
				if (s != null)
					s.close();
			} catch(IOException e) {
				log.warn("IOException during close", e);
			}
		}
	}
}
