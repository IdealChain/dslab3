package at.tuwien.group115.client.offline;

import java.io.IOException;
import java.net.SocketException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.ArrayList;

import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;

import at.tuwien.group115.shared.BidCommand;
import at.tuwien.group115.shared.BidFormatIllegalException;
import at.tuwien.group115.shared.security.ClientCredentials;
import at.tuwien.group115.shared.security.IAuthenticateClient;
import at.tuwien.group115.shared.security.ISecureChannel;
import at.tuwien.group115.shared.security.LoopbackChannel;

/**
 * ISecureChannel which supports going into a transparent offline mode.
 * 
 * Will switch to offline mode when an underlying socket gets closed, and 
 * check regularily in the background whether the server has become 
 * available again.
 * 
 * You should lock on this channel instance to ensure the background thread
 * does not interfere between your send() and receive() calls.
 */
public class OfflineModeChannel implements ISecureChannel {
	
	private final static Logger log = Logger.getLogger(OfflineModeChannel.class);
	
	/**
	 * Callback that yields a new secure channel.
	 */
	private ISecureChannelCreator channelCreator;
	
	/**
	 * Underlying secure channel.
	 * Only defined when online, null when offline.
	 */
	private ISecureChannel channel;
	
	/**
	 * The cached client credentials for logging in again later.
	 */
	private ClientCredentials cachedClientCredentials = null;
	
	/**
	 * Loopback channel to queue the pseudo responses to be returned in offline mode.
	 */
	private LoopbackChannel loopbackChannel = null;
	
	/**
	 * Thread which checks regularily whether the server is available or not,
	 * and updates the list of known clients.
	 */
	private OfflineCheckThread checkThread = null;
	
	/**
	 * Thread which opens a server socket which signs timestamp requests for other
	 * clients.
	 */
	private TimestampProviderThread timestampThread = null;
	
	/**
	 * Known other clients which are currently also logged in on the server.
	 */
	private ActiveClients otherClients = null;
	
	/**
	 * Signed bids pending for submission when the server comes back online.
	 */
	private ArrayList<SignedBid> signedOfflineBids = null;
	
	public OfflineModeChannel(ISecureChannelCreator creator) throws IOException {
		this.channelCreator = creator;
		this.channel = creator.createSecureChannel();
	}
	
	/**
	 * Gets whether this channel is currently in offline mode.
	 */
	public synchronized boolean isOffline() {		
		return channel == null;
	}
	
	private synchronized String enterOffline(String cause) {
		
		assert !this.isOffline() : "already offline";
		
		this.channel = null;
		this.loopbackChannel = new LoopbackChannel();
		
		String msg = String.format("Connection lost, entered offline mode (%s), %d other clients", cause, otherClients.count()); 
		log.info(msg);
		return msg;
		
	}
	
	protected synchronized void enterOnline(ISecureChannel channel) {
		
		assert this.isOffline() : "already online";
		assert channel.isSecure() : "insecure channel not valid";
		
		log.info("Connection regained, online.");
		this.channel = channel;
		
		// send pending signed bids
		try {
			if (this.signedOfflineBids.size() > 0) {
				
				for (SignedBid b : this.signedOfflineBids) {
					log.info(String.format("Submitting: %s", b.toString()));
					this.send(b.toString());
					log.info(this.receive());
				}
				
				this.signedOfflineBids.clear();
			}
		} catch (IOException e) {
			log.warn("IOException during sending of signedBids", e);
		}
	}
	
	protected synchronized void parseActiveClientsList(String list) {	
		otherClients.parse(list);
	}
	
	@Override
	public synchronized boolean login(ClientCredentials cred)
		throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {
		
		if (channel.login(cred)) {
			log.info(String.format("Login successful, saving '%s' credentials for later...", cred.getUsername()));
			this.cachedClientCredentials = cred;
			
			// create active clients list which will be populated by the background thread
			this.otherClients = new ActiveClients(cred.getUsername());
			this.signedOfflineBids = new ArrayList<SignedBid>();
			
			// start background checking thread
			this.checkThread = new OfflineCheckThread(this, channelCreator, cred);
			this.checkThread.start();
			
			// start timestamp provider thread
			this.timestampThread = new TimestampProviderThread(cred);
			this.timestampThread.start();
			
			return true;
		}
		
		return false;
	}

	@Override
	public synchronized boolean initServer(String msg, PrivateKey myKey, IAuthenticateClient auth)
		throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException {

		throw new RuntimeException("Offline mode not supported for server.");
	}
	
	@Override
	public synchronized void reset() {		
		
		// stop background checking thread
		if (this.checkThread != null) {
			this.checkThread.stop();
			this.checkThread = null;
		}
		
		// stop timestamp provider thread
		if (this.timestampThread != null) {
			this.timestampThread.stop();
			this.timestampThread = null;
		}
		
		this.loopbackChannel = null;
		this.cachedClientCredentials = null;
		this.otherClients = null;
		this.signedOfflineBids = null;
		
		if (!isOffline())
			this.channel.reset();
	}
	
	@Override
	public synchronized boolean isSecure() {
		
		if (isOffline())
			return true;
		
		return channel.isSecure();
	}

	@Override
	public synchronized void close() throws IOException {
		if (channel != null)
			channel.close();
	}

	@Override
	public synchronized String receive() throws IOException {
		
		if (this.isOffline()) {
			if (this.loopbackChannel != null)
				return this.loopbackChannel.receive();
			else
				return null;
		}
		
		String msg = null;
		
		try {
			msg = channel.receive();
			if (msg == null && this.isSecure())
				return this.enterOffline("null response");
			
		} catch (SocketException e) {
			if (this.isSecure())
				return this.enterOffline(e.getMessage());
			
			throw e;
		}
		
		return msg;
	}

	@Override
	public synchronized void send(String msg) throws IOException {
		
		if (this.isOffline()) {
			
			try {
				// try to create signed offline bid
				SignedBid bid = new SignedBid(new BidCommand("!bid", msg), otherClients);
				loopbackChannel.send(bid.getTimestamps());
				
				if (bid.isSigned())
					this.signedOfflineBids.add(bid);
				
				return;
				
			} catch (BidFormatIllegalException e) {}
			
			loopbackChannel.send("Supported commands in offline mode: !bid !logout !exit");
			return;
		}
		
		try {
			channel.send(msg);
		} catch (SocketException e) {
			loopbackChannel.send(this.enterOffline(e.getMessage()));
		}
	}

	@Override
	public synchronized String getUsername() {
		if (this.isOffline()) {
			if (cachedClientCredentials != null)
				return cachedClientCredentials.getUsername();
			
			return null;
		}
		
		return channel.getUsername();
	}

}
