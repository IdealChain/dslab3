package at.tuwien.group115.client.offline;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import org.apache.log4j.Logger;

import at.tuwien.group115.shared.BidCommand;
import at.tuwien.group115.shared.BidFormatIllegalException;
import at.tuwien.group115.shared.security.ClientCredentials;
import at.tuwien.group115.shared.security.SignatureHelper;
import at.tuwien.group115.shared.security.TCPChannel;

public class TimestampProviderThread implements Runnable {
	
	private final static Logger log = Logger.getLogger(TimestampProviderThread.class);
	
	private ClientCredentials cred;
	private boolean isStopped = false;
	private ServerSocket serverSocket = null;
	
	public TimestampProviderThread(ClientCredentials cred) {
		this.cred = cred;
	}
	
	public Thread start() {
		Thread t = new Thread(this);
		t.setDaemon(true);
		t.start();
		return t;
	}

	@Override
	public void run() {
		openServerSocket(cred.getClientPort());
		
		try {
			
			while (!this.isStopped()) {
				
				Socket clientSocket = null;
				
				try {
					clientSocket = serverSocket.accept();
					
				} catch (IOException e) {
					if(this.isStopped()) 
						break;
					
					log.error("Error accepting client connection: " + e.toString());
					break;
				}
				
				provideTimestamp(clientSocket);
			}
			
		} finally {
			try {
			if (serverSocket != null)
				serverSocket.close();
			} catch (IOException e) {
				log.warn(e);
			}
		}
		
		log.info("Stopped.");
	}
	
	private void provideTimestamp(Socket clientSocket) {
		TCPChannel channel = null;
		BidCommand request = null;
		
		try {
			channel = new TCPChannel(clientSocket);
			
			try {
				request = new BidCommand("!getTimestamp", channel.receive());
			} catch (BidFormatIllegalException e) {
				log.warn(e.getMessage());
				channel.send(e.getMessage());
				return;
			} 
			
			log.info("Got timestamp request: " + request.getGetTimestamp());
			
			try {				
				String response = request.getTimestamp(System.currentTimeMillis());
				response += " " + SignatureHelper.createSignature(response, this.cred.getMyKey());
				
				log.info("Generated timestamp response:\n" + response);
				channel.send(response);
				
			} catch (NoSuchAlgorithmException e) {
				log.error(e);
				channel.send(e.getMessage());
				return;
			} catch (InvalidKeyException e) {
				log.error(e);
				channel.send(e.getMessage());
				return;
			} catch (SignatureException e) {
				log.error(e);
				channel.send(e.getMessage());
				return;
			}
			
		} catch (Exception e) {
			log.warn("Error while providing timestamp", e);
		} finally {
			try {
			if (channel != null)
				channel.close();
			} catch (IOException e) {
				log.warn(e);
			}
		}
	}
	
	public synchronized boolean isStopped() {
	    return this.isStopped;
	}	
	
	public synchronized void stop() {
		this.isStopped = true;
		try {
			if (this.serverSocket != null) {
				this.serverSocket.close();
				this.serverSocket = null;
			}
		} catch (IOException e) {
			log.error("Error closing server socket", e);
		}
	}
	
	private synchronized void openServerSocket(int port) {
		try {
			this.serverSocket = new ServerSocket(port);
			log.info(String.format("Started server socket (%d)", port));
		} catch (IOException e) {
			log.fatal("Cannot bind to port", e);
		}
	}

}
