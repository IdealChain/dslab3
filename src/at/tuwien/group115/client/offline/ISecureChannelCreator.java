package at.tuwien.group115.client.offline;

import java.io.IOException;

import at.tuwien.group115.shared.security.ISecureChannel;

public interface ISecureChannelCreator {
	
	public ISecureChannel createSecureChannel() throws IOException;

}
