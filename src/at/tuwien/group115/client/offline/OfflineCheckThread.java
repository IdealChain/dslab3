package at.tuwien.group115.client.offline;

import java.io.IOException;

import org.apache.log4j.Logger;

import at.tuwien.group115.shared.security.ClientCredentials;
import at.tuwien.group115.shared.security.ISecureChannel;

public class OfflineCheckThread implements Runnable {
	
	private final static Logger log = Logger.getLogger(OfflineCheckThread.class);
	
	private boolean isStopped = false;
	
	private OfflineModeChannel parent;
	private ISecureChannelCreator channelCreator;
	private ClientCredentials cred;
	
	public OfflineCheckThread(OfflineModeChannel parent, ISecureChannelCreator creator, ClientCredentials cred) {
		this.parent = parent;
		this.channelCreator = creator;
		this.cred = cred;
	}
	
	public Thread start() {
		Thread t = new Thread(this);
		t.setDaemon(true);
		t.start();
		return t;
	}

	@Override
	public void run() {
		log.info("Started");
		
		while (!this.isStopped()) {
			
			// we lock on the parent channel while working
			synchronized(this.parent) {
				
				if (parent.isOffline()) {
					// test whether server is back up
					ISecureChannel channel = tryCreateSecureChannel();
					if (channel != null)
						parent.enterOnline(channel);
					
				} else {
					// get client list from server
					String list = getClientList();
					if (list != null && !parent.isOffline())
						parent.parseActiveClientsList(list);
					
				}
			}
			
			if (this.isStopped())
				break;
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				log.warn(e);
			}
		}
		
		log.info("Stopped");
	}
	
	/**
	 * Test whether the server is reachable again.
	 */
	private ISecureChannel tryCreateSecureChannel() {

		try {
			ISecureChannel channel = channelCreator.createSecureChannel();
			if (channel.login(cred)) {
				String resp = channel.receive(); // consume login message
				if (!resp.startsWith("Error"))
					return channel;
				
				log.warn("Handshake successful, but error on re-login: " + resp);
			}
		} catch (Exception e) {	}
		
		return null;
	}
	
	/**
	 * Gets the current list of clients from the server.
	 */
	private String getClientList() {
		try {
			parent.send("!getClientList");
			return parent.receive();
		} catch (IOException e) {
			log.warn(e);
		}
		return null;
	}
	
	public synchronized boolean isStopped() {
	    return this.isStopped;
	}	
	
	public synchronized void stop() {
	    this.isStopped = true;
	}

}
