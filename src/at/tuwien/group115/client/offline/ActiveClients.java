package at.tuwien.group115.client.offline;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class ActiveClients {
	
	private final static Logger log = Logger.getLogger(ActiveClients.class);
	private final static Pattern listRowPattern = Pattern.compile("^((?:[0-9]{1,3}\\.){3}[0-9]{1,3}):([0-9]{1,5}) ?- ?([a-zA-Z0-9_\\-]+)$");
	
	private ArrayList<ActiveClient> clients = new ArrayList<ActiveClient>();
	private Random random = new Random();
	private String myUsername;

	public ActiveClients(String myUsername) {
		this.myUsername = myUsername;
	}
	
	public synchronized void parse(String list) {
		
		clients.clear();
		
		for (String row : list.split("\n")) {
			Matcher matcher = listRowPattern.matcher(row);
			try {
				if (matcher.matches()) {
					InetAddress addr = InetAddress.getByName(matcher.group(1));
					int clientPort = Integer.parseInt(matcher.group(2));
					String username = matcher.group(3);
					
					if (!username.equals(myUsername))
						clients.add(new ActiveClient(addr, clientPort, username));
				}
			} catch (NumberFormatException e) {
				log.warn("Invalid port number " + matcher.group(2));
			} catch (UnknownHostException e) {
				log.warn("Unknown host: " + matcher.group(1));
			}
		}
	}
	
	public synchronized int count() {
		return clients.size();
	}
	
	/**
	 * Returns a new list with the currently known clients in random order.
	 */
	public synchronized ArrayList<ActiveClient> shuffle() {
		ArrayList<ActiveClient> shuffled = new ArrayList<ActiveClient>();
		
		while (shuffled.size() < clients.size()) {
			
			ActiveClient client = clients.get(random.nextInt(clients.size()));
			if (!shuffled.contains(client))
				shuffled.add(client);
		}
		
		return shuffled;
	}
}
