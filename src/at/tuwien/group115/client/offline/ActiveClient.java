package at.tuwien.group115.client.offline;

import java.net.InetAddress;

public class ActiveClient {

	protected InetAddress address;
	protected int clientPort;
	protected String username;
	
	public ActiveClient(InetAddress address, int clientPort, String username) {
		this.address = address;
		this.clientPort = clientPort;
		this.username = username;
	}

	public InetAddress getAddress() {
		return address;
	}

	public int getClientPort() {
		return clientPort;
	}

	public String getUsername() {
		return username;
	}
	
	@Override
	public String toString() {
		return String.format("%s:%d - %s", address.toString(), clientPort, username);
	}
	
}
