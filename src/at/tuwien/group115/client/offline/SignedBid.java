package at.tuwien.group115.client.offline;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import at.tuwien.group115.shared.BidCommand;
import at.tuwien.group115.shared.Signature;
import at.tuwien.group115.shared.security.TCPChannel;

public class SignedBid {
	
	private final static Logger log = Logger.getLogger(SignedBid.class);
	
	private final static String B64 = "a-zA-Z0-9/+";
	private final static Pattern timestampPattern = Pattern.compile("^!timestamp ([0-9]+) ([0-9\\.]+) ([0-9]+) (["+B64+"]+=+)$");
	private final static int neededSignatures = 2;
	
	private BidCommand bid;
	private ActiveClients clients;
	
	private String sigs = "";
	private boolean signed = false;

	public SignedBid(BidCommand bid, ActiveClients clients) {
		this.bid = bid;
		this.clients = clients;
	}

	public String getTimestamps() {
		ArrayList<ActiveClient> s = clients.shuffle();
		String request = bid.getGetTimestamp();
		int obtained = 0;
		
		log.info(String.format("Contacting %d of %d known clients...", neededSignatures, s.size()));
		
		while (obtained < neededSignatures) {
			if (s.size() < 1)
				return String.format("Error: There were not enough reachable clients (%d/%d).", obtained, neededSignatures);
			
			ActiveClient client = s.remove(0);
			TCPChannel channel = null;
					
			try {
				channel = new TCPChannel(new Socket(client.getAddress(), client.getClientPort()));
				channel.send(request);
				
				Matcher timestamp = timestampPattern.matcher(channel.receive());
				if (!timestamp.matches()) {
					log.warn("Returned timestamp did not match pattern: " + timestamp.toString());
					continue;
				}
				
				Signature sig = new Signature(client.getUsername(), timestamp.group(3), timestamp.group(4));
				log.info("Obtained signature: " + sig);
				
				if (!sigs.isEmpty())
					sigs += " ";
				sigs += sig.toString();
				obtained++;
				
			} catch (Exception e) {
				log.warn("Exception during getTimestamp", e);
			} finally {
				try {
					if (channel != null)
						channel.close();
				} catch (IOException e) {
					log.warn(e);
				}
			}
		}
		
		this.signed = true;
		return String.format("Your bid (%s) has been signed and will be placed when the server is reachable again.", bid.toString());
	}
	
	public boolean isSigned() {
		return signed;
	}

	@Override
	public String toString() {
		return bid.getSignedBid(sigs);
	}
}