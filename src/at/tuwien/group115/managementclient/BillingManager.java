package at.tuwien.group115.managementclient;

import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import at.tuwien.group115.billing.IBillingServer;
import at.tuwien.group115.billing.IBillingServerSecure;
import at.tuwien.group115.shared.PropertiesInvalidException;
import at.tuwien.group115.shared.RegistryHelper;

public class BillingManager {

	private final static Logger log = Logger.getLogger(BillingManager.class);
	
	private String billingBindingName;
	private IBillingServer server;
	private AtomicInteger currentId = new AtomicInteger(0);

	private Hashtable<Integer, BillingCallback> subscriptions = new Hashtable<Integer, BillingCallback>();
	
	public BillingManager(String billingBindingName) {
		this.billingBindingName = billingBindingName;
	}
	
	public synchronized IBillingServerSecure login(String username, String password) {
		if (this.server == null)
			getRemoteServer();
		
		if (this.server == null) {
			log.warn("Can't subscribe - billing server not running");
			return null;
		}
		
		IBillingServerSecure sec = null;
		try {
			sec = server.login(username, password);
		} catch (RemoteException e) {
			log.warn("Registry could not be obtained", e);
		}
		if (sec != null)
		{
			BillingCallback callback = null;
			try {
				callback = new BillingCallback(this);
			} catch (RemoteException e) {
				log.warn("Registry could not be obtained", e);
			}
			int id = currentId.getAndIncrement();
			subscriptions.put(id, callback);
			
			log.info(String.format("User %s successfully logged in", username));
			return sec;
		}
		return null;
	}
	
	private void getRemoteServer() {
		
		// obtain RMI registry
		Registry registry = null;
		try {
			registry = RegistryHelper.getRegistry();
		} catch (PropertiesInvalidException e) {
			log.warn("registry.properties file invalid: " + e.getMessage(), e.getCause());
			return;
		} catch (RemoteException e) {
			log.warn("Registry could not be obtained", e);
			return;
		}
		
		// obtain remote object
		Remote remoteObject;
		try {
			remoteObject = registry.lookup(this.billingBindingName);
			if (remoteObject instanceof IBillingServer) {
				server = (IBillingServer)remoteObject;
				log.info("Remote billing server obtained successfully");
			} else {
				log.warn("Remote object not an billing server");
			}
			
		} catch (NotBoundException e) {
			log.warn("No billing server class bound in registry");
		} catch (RemoteException e) {
			log.warn("Error during lookup of billing server", e);
		}
	}
	
	
	public void cleanup() {
		if (subscriptions.size() > 0)
			log.info(String.format("Cleaning %d open subscriptions...", subscriptions.size()));
		
		for(Map.Entry<Integer, BillingCallback> entry : subscriptions.entrySet()) {
			unsubscribe(entry.getKey());
			try {
				UnicastRemoteObject.unexportObject(entry.getValue(), true);
			} catch (NoSuchObjectException e) {}
		}
		
		subscriptions.clear();
	}
	
	public boolean unsubscribe(Integer id) {
		
		if (this.server == null)
			getRemoteServer();
		
		if (this.server == null) {
			log.warn("Can't unsubscribe - billing server not running");
			return false;
		}
		
		try {
			BillingCallback callback = subscriptions.remove(id);
			
			if (callback == null)
				return false;
			
			UnicastRemoteObject.unexportObject(callback, true);
			return true;
			
		} catch (RemoteException e) {
			log.warn("Remote error during subscribing", e);
			this.server = null;
		}
		
		return false;
	}
}
