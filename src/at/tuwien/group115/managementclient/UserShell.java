package at.tuwien.group115.managementclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import at.tuwien.group115.billing.Bill;
import at.tuwien.group115.billing.IBillingServerSecure;
import at.tuwien.group115.billing.PriceStep;
import at.tuwien.group115.shared.InsufficientParametersException;
import at.tuwien.group115.shared.NotLoggedInException;

public class UserShell {

	private final static Logger log = Logger.getLogger(UserShell.class);
	
	private SubscriptionManager subManager;
	private BillingManager billManager;
	private IBillingServerSecure sec;
	private boolean isActive = true;
	
	protected String user;
	
	public UserShell(SubscriptionManager subManager, BillingManager billManager) {
		this.subManager = subManager;	
		this.billManager = billManager;
	}
	
	public void run() {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out, true);
		
		try {
		
			String cmd;
			System.out.println("ManagementClient ready.");
			System.out.println(hide());
			
			do {
				
				if (user != null)
					out.print(user);
				
				out.print("> ");
				out.flush();
				
				if ((cmd = in.readLine()) == null)
					break;
				
				out.println(executeCommand(cmd));
				
			} while (this.isActive);
		
		} catch (Exception e) {
			log.fatal("Error", e);
			
		} finally {
			try {
				// cleanup all subscriptions
				subManager.cleanup();
				billManager.cleanup();
				
				in.close();
				out.close();
			} catch(IOException e) {
				log.warn("IOException during close", e);
			}
		}
	}
	
	private String executeCommand(String cmd) {
		
		try {
			if (cmd.startsWith("!subscribe")) {
				return subscribe(extractParams(cmd, 1));
			} else if (cmd.startsWith("!unsubscribe")) {
				return unsubscribe(extractParams(cmd, 1));
			} else if (cmd.startsWith("!print")) {
				return print();
			} else if (cmd.startsWith("!auto")) {
				return auto();
			} else if (cmd.startsWith("!hide")) {
				return hide();
			} else if (cmd.startsWith("!end") || cmd.startsWith("!exit")) {
				return end();
			} else if (cmd.startsWith("!login")) {
				return login(extractParams(cmd, 2));
			} else if (cmd.startsWith("!steps")) {
				return steps();
			} else if (cmd.startsWith("!addStep")) {
				return addStep(extractParams(cmd, 4));
			} else if (cmd.startsWith("!removeStep")) {
				return removeStep(extractParams(cmd, 2));
			} else if (cmd.startsWith("!bill")) {
				return bill(extractParams(cmd, 1));
			} else if (cmd.startsWith("!logout")) {
				return logout();
			} else if (cmd.isEmpty()) {
				return "Please give a command.";
			} else {
				return "Unknown command: " + cmd;
			}			
		} catch (NotLoggedInException e) {
			return "Error: you must login first!";
		} catch (InsufficientParametersException e) {
			return String.format("Error: expected %d arg(s)!", e.ExpectedParameters);
		}
	}

	private String login(String[] args) {
		sec = billManager.login(args[1], args[2]);
		if (sec != null)
		{
			user = args[1];
			return String.format("%s successfully logged in", args[1]);
		}
		
		return String.format("%s not logged in", args[1]);
	}
	
	private String addStep(String[] args) throws NotLoggedInException {
		if (sec == null)
			throw new NotLoggedInException();

		try {
			sec.createPriceStep(
					Double.parseDouble(args[1]), 
					Double.parseDouble(args[2]),
					Double.parseDouble(args[3]), 
					Double.parseDouble(args[4]));
		} catch (NumberFormatException e) {
			log.error("Error at parsing values");
			return String.format("Could not add step [%s %s]: the number format is not supported", args[1], args[2]);
		} catch (RemoteException e) {
			log.error("Could not create a new step", e);
			return String.format("Could not add step [%s %s]: step already exists", args[1], args[2]);
		}
		if (args[2].equals("0"))
			return String.format("Step [%s INFINITY] successfully added", args[1]);
		
		return String.format("Step [%s %s] successfully added", args[1], args[2]);
	}
	
	private String removeStep(String[] args) throws NotLoggedInException {
		if (sec == null)
			throw new NotLoggedInException();
		
		try {
			sec.deletePriceStep(Double.parseDouble(args[1]), Double.parseDouble(args[2]));
		} catch (NumberFormatException e) {
			log.error("ERROR: Error while parsing values");
			return "ERROR: Error while parsing values";
		} catch (RemoteException e) {
			log.error(String.format("ERROR: Price step [%s, %s] does not exist", args[1], args[2]));
			return String.format("ERROR: Price step [%s, %s] does not exist", args[1], args[2]);
		}
		return String.format("Price step [%s, %s] successfully removed", args[1], args[2]);
	}
	
	private String logout() throws NotLoggedInException {
		if (sec == null)
			throw new NotLoggedInException();
		
		sec = null;
		user = null;
		
		return "Successfully logged out";
	}
	
	private String steps() throws NotLoggedInException {
		if (sec == null)
			throw new NotLoggedInException();
		
		HashMap<Double, PriceStep> steps = null;
		try {
			steps = sec.getPriceSteps().getPriceSteps();
		} catch (RemoteException e) {
			log.warn("Could not fetch steps", e);
		}

		String s = "\tMin_Price\tMax_Price\tFee_Fixed\tFee_Variable\n";
		for (PriceStep step : steps.values())
		{
			if (step.getEnd().equals(Double.valueOf("0")))
				s += String.format("\t\t%9.2f\t INFINITY\t%9.2f\t%10.1f %%\n", 
						step.getStart(), step.getFixedPrice(), step.getvariablePricePercent());
			else
				s += String.format("\t\t%9.2f\t%9.2f\t%9.2f\t%10.1f %%\n", 
						step.getStart(), step.getEnd(), step.getFixedPrice(), step.getvariablePricePercent());
		}
		return s;
	}
	
	private String bill(String[] args) throws NotLoggedInException {
		if (sec == null)
			throw new NotLoggedInException();
		
		Bill bill = null;
		try {
			 bill = sec.getBill(args[1]);
		} catch (RemoteException e) {
			log.warn("Could not load bill");
		}
		if (bill != null)
			return bill.toString();
		return String.format("No bill for user %s found", args[1]);
	}
	
	private String subscribe(String [] args) {
		String id = subManager.subscribe(args[1]);
		
		if (id == null)
			return "Could not create subscription - is analytics server running?";
		
		return String.format("Created subscription with ID %s for events using filter %s", id, args[1]);
	}
	
	private String unsubscribe(String [] args) {
		boolean success = subManager.unsubscribe(args[1]);
		if (success)
			return String.format("Subscription %s terminated", args[1]);
		
		return String.format("Subscription %s could not be terminated", args[1]);
	}
	
	private String print() {
		int printed = subManager.printPendingEvents();
		if (printed == 0)
			return "No events pending.";
		return "";
	}
	
	private String auto() {
		subManager.setAutoPrintingMode(true);
		return "Automatic printing of events enabled";
	}
	
	private String hide() {
		subManager.setAutoPrintingMode(false);
		return "Automatic printing of events disabled";
	}
	
	private String end() {
		this.isActive = false;
		return "Bye!";
	}
	
	private static String[] extractParams(String msg, int paramCount) throws InsufficientParametersException {
		
		String[] params = msg.split(" ", paramCount + 1);
		if (params.length < paramCount + 1)
			throw new InsufficientParametersException(paramCount);
		
		return params;
	}
}
