package at.tuwien.group115.managementclient;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import at.tuwien.group115.analytics.IAnalyticsEventCallback;
import at.tuwien.group115.events.Event;

public class SubscriptionCallback extends UnicastRemoteObject implements IAnalyticsEventCallback {

	private static final long serialVersionUID = 1L;
	private SubscriptionManager parent;
	
	public SubscriptionCallback(SubscriptionManager parent) throws RemoteException {
		this.parent = parent;
	}
	
	@Override
	public void processEvent(Event event) throws RemoteException {
		parent.eventReceived(event);
	}

}
