package at.tuwien.group115.managementclient;

import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.apache.log4j.Logger;

import at.tuwien.group115.analytics.IAnalyticsServer;
import at.tuwien.group115.events.Event;
import at.tuwien.group115.shared.PropertiesInvalidException;
import at.tuwien.group115.shared.RegistryHelper;

public class SubscriptionManager {

	private final static Logger log = Logger.getLogger(SubscriptionManager.class);
	
	private String analyticsBindingName;
	private IAnalyticsServer server;
	
	private boolean autoPrintingMode = false;
	private Hashtable<String, SubscriptionCallback> subscriptions = new Hashtable<String, SubscriptionCallback>();
	private Queue<Event> pendingEvents = new LinkedList<Event>();
	private String lastReceivedEventId;

	public SubscriptionManager(String analyticsBindingName) {
		this.analyticsBindingName = analyticsBindingName;
	}
	
	public String subscribe(String filterRegex) {
		
		if (this.server == null)
			getRemoteServer();
		
		if (this.server == null) {
			log.warn("Can't subscribe - no analytics server object");
			return null;
		}
		
		try {
			SubscriptionCallback callback = new SubscriptionCallback(this);
			String id = server.subscribe(callback, filterRegex);
			subscriptions.put(id, callback);
			return id;
			
		} catch (RemoteException e) {
			log.warn("Remote error during subscribing", e);
			this.server = null;
		}
		
		return null;
	}
	
	public boolean unsubscribe(String id) {
		
		if (this.server == null)
			getRemoteServer();
		
		if (this.server == null) {
			log.warn("Can't unsubscribe - no analytics server object");
			return false;
		}
		
		try {
			SubscriptionCallback callback = subscriptions.remove(id);
			
			if (callback == null)
				return false;
			
			boolean success = server.unsubscribe(id);
			UnicastRemoteObject.unexportObject(callback, true);
			return success;
			
		} catch (RemoteException e) {
			log.warn("Remote error during subscribing", e);
			this.server = null;
		}
		
		return false;
	}
	
	public void cleanup() {
		if (subscriptions.size() > 0)
			log.info(String.format("Cleaning %d open subscriptions...", subscriptions.size()));
		
		for(Map.Entry<String, SubscriptionCallback> entry : subscriptions.entrySet()) {

			// tell the server, if possible
			try {
				server.unsubscribe(entry.getKey());
			} catch (RemoteException e) {}
			
			try {
				UnicastRemoteObject.unexportObject(entry.getValue(), true);
			} catch (NoSuchObjectException e) {}
		}
		
		subscriptions.clear();
	}
	
	protected void eventReceived(Event event) {
		
		synchronized(this) {
			// filter out multiple successive instances of the same event id
			if (this.lastReceivedEventId != null && event.id.equals(this.lastReceivedEventId))
				return;
			
			this.lastReceivedEventId = event.id;
		}
		
		// print immediately
		if (this.getAutoPrintingMode()) {
			System.out.println(event.toManagementString());
			return;
		}
		
		// queue for later printing
		synchronized(pendingEvents) {
			pendingEvents.offer(event);
		}
	}
	
	public int printPendingEvents() {
		int count = 0;
		synchronized(pendingEvents) {
			Event event;
			while ((event = pendingEvents.poll()) != null) {
				System.out.println(event.toManagementString());
				count++;
			}
		}
		return count;
	}
	
	public synchronized boolean getAutoPrintingMode() {
		return autoPrintingMode;
	}
	
	public synchronized void setAutoPrintingMode(boolean value) {
		autoPrintingMode = value;
		
		// empty queue when switching to auto mode
		if (value)
			printPendingEvents();
	}
	
	private void getRemoteServer() {
		
		// obtain RMI registry
		Registry registry = null;
		try {
			registry = RegistryHelper.getRegistry();
		} catch (PropertiesInvalidException e) {
			log.warn("registry.properties file invalid: " + e.getMessage(), e.getCause());
			return;
		} catch (RemoteException e) {
			log.warn("Registry could not be obtained", e);
			return;
		}
		
		// obtain remote object
		Remote remoteObject;
		try {
			remoteObject = registry.lookup(this.analyticsBindingName);
			if (remoteObject instanceof IAnalyticsServer) {
				server = (IAnalyticsServer)remoteObject;
				log.info("Remote analytics server obtained successfully");
			} else {
				log.warn("Remote object not an analytics server");
			}
			
		} catch (NotBoundException e) {
			log.warn("No analytics server class bound in registry");
		} catch (RemoteException e) {
			log.warn("Error during lookup of analytics server", e);
		}
	}
}
