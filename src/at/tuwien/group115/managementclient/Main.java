package at.tuwien.group115.managementclient;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class Main {

	private final static Logger log = Logger.getLogger(Main.class);
	
	public static void main(String[] args) {

		if (args.length < 2 || args[0].isEmpty() || args[1].isEmpty())
			usage();
		
		// configure basic log4j console logger
		BasicConfigurator.configure();
		
		SubscriptionManager subManager = new SubscriptionManager(args[0]);
		BillingManager billManager = new BillingManager(args[1]);
		UserShell shell = new UserShell(subManager, billManager);
		shell.run();
	}
	
	public static void usage() {
		log.error(String.format(
				"Usage: java %s <analytics-bindingName> <billing-bindingName>", 
				Main.class.getName()));
		System.err.println(String.format(
				"Usage: java %s <analytics-bindingName> <billing-bindingName>", 
				Main.class.getName()));
		System.exit(1);
	}

}
