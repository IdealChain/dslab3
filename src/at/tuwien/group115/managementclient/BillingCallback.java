package at.tuwien.group115.managementclient;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import at.tuwien.group115.billing.IBillingServer;
import at.tuwien.group115.billing.IBillingServerSecure;

public class BillingCallback extends UnicastRemoteObject implements IBillingServer {

	private static final long serialVersionUID = 1L;
	private BillingManager parent;
	
	public BillingCallback(BillingManager parent) throws RemoteException {
		this.parent = parent;
	}

	@Override
	public IBillingServerSecure login(String username, String password)
			throws RemoteException {
		return parent.login(username, password);
	}
}
